## Run the server

*  Need the same requirements as traverse editorial platform, just a running mongo database
*  Install all dependencies

```bash
npm install
```

*  Easy part, just run the server (no need to recompile the whole backend every time, just load all files on the fly)

```bash
npm run start:local #On your computer
```