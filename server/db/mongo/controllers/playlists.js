/**
 * Created by hasj on 21/11/2016.
 */
import Playlist from '../models/playlists';
import LinkedFiche from '../models/linkedFiches';
import Fiche from '../models/fiches';
import User from '../models/users';

import { PAGINATION_DEFAULT, status } from '../constant';
import { CONSTANTS, createSkipQuery, applyModelVersion, mixinStat, mixinLike, mixinDislike } from '../static';
/**
 * List
 */
//TODO, need to check if the user has enough privileges to edit a playlist nor user exists while creating or updating a playlist

function userHasEnoughPrivileges(userID){
  return false;
}

export function all(req, res) {

  let query = { status: { $in: [0, 1] }},
    body = req.query,
    limit = null,
    skipQuery = null,
    page = null;

  if(Object.keys(body).length > 0) {

    if (body.limit) {
      limit = parseInt(body.limit);
    }

    if (body.page) {
      page = parseInt(body.page);
      skipQuery = createSkipQuery(page, limit);
    }
    //checking user id privileges also
    //checking pagination
  }

  //TODO check the below find query statement
  let toExec = Playlist.find(query)
    .populate([{ // populate the field which is another mongo Object
      path:'linkedFiches', //name of the field
      model: 'LinkedFiche', // the object I would like to get
    },{
      path: 'created_by',
      model: 'User'
    }]);

  if (skipQuery !== null) {
    toExec = toExec.skip(skipQuery);
  }

  toExec.limit(limit).sort({created_at: -1}).exec((err, playlists ) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return prePopulate(req, res, playlists, true);
  });
}


//need to be able to add a fiche directly from here
export function add(req, res){
    let {body} = req,
        ficheId = body.ficheId,
        playlist = new Playlist({
          name: body.name ? body.name : '',
          description: body.description ? body.description : '',
          created_by: body.created_by ? body.created_by : null,
          cover: { path: body.photoUrl ? body.photoUrl : ''},
          labels: body.labels ? body.labels : null,
          status: body.status ? parseInt(body.status) : status.DRAFT
      });

    //cannot add a playlist without a creation user
    if (!playlist.created_by) {
      return res.status(500).send("user does not exists");
    }
    User.findOne({_id: body.created_by}, (err, author) => {
      if (err){
        return res.status(500).send("user does not exists");
      }

      playlist.created_by = author;

      playlist.save((err) => {
        if(err){
            console.log(err);
            res.status(500).send("playlist not created");
        }

        //in all cases we just try to find a fiche, if does not exist just don't care and follow flow
        return Fiche.findOne({_id: ficheId}, (err, fiche) => {
          if(err){
            res.status(500).send("Fiche does not exists");
          }

          if (fiche) {
            //small tweak to allow fiche to be added
            req.body.fiche = fiche;
            req.params.id = playlist._id;
            return addLink(req, res);
          }else {
            return prePopulate(req, res, playlist, false);
          }
        });


      });
    });
}

/*
 * Will expect an array of fiches Ids in order to update the swap order
 * Will return the updated playlist
 */
export function swap(req, res) {
    return Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
      if (err) {
        return res.status(500).send("playlist not found");
      }

      const { ficheIds } = req.body;

      if (!ficheIds) {
        return res.status(401).send("need some fiches ids");
      }

      //need to populate these since we just have array of fichesIds
      Fiche.find({_id: {$in: ficheIds}}, (err, receivingFiches) => {
        if (err) {
          return res.status(401).send("some fiches do not exist");
        }

        //reverse order
        receivingFiches.sort((a, b) => {
          //need to cast new objectId to string, otherwise the comparison won't work
          return ficheIds.indexOf(b._id.toString()) - ficheIds.indexOf(a._id.toString());
        });

        Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
          populate: {
            path: 'fiche', model: 'Fiche'
          }
        }, function (err, populated) {
            if (err) {
                res.status(400).send("Populating failed");
            }


          let { linkedFiches } = populated,
            remoteIndex = 0;

          //removing from database the current linkedFiches (check if no side effect)
          linkedFiches.remove();

          let previousLF = null;

          let updatedLinkedFiches = receivingFiches.map((fiche) => {
            let newLF = new LinkedFiche({
                fiche: fiche,
                value: null,
                next: previousLF
            });

            newLF.save();

            previousLF = newLF;
            return newLF;
          });

          //now need to reverse back the array ;) since we need to respect the order of array given by the user
          updatedLinkedFiches.sort((a, b) => {
            return ficheIds.indexOf(a.fiche._id.toString()) - ficheIds.indexOf(b.fiche._id.toString());
          });

          playlist.linkedFiches = updatedLinkedFiches;

          //mandatory since we alter also the length of the linkedFiche array
          playlist.save((err) => {
            if (err) {
              res.status(500).send("Updating the playlist failed");
            }

            return prePopulate(req, res, playlist, false);
          });


        });
      })
    });
}

export function deleteFiche(req, res) {
  Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
    if (err) {
        res.status(500).send("playlist not found");
    }

    Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
        populate: {
            path: 'fiche', model: 'Fiche'}
    }, function (err, playlist) {
      if (err) {
          res.status(401).send("Populating failed");
      }

      var ficheToRemoveId = req.body.linkedFicheId;
      var idToRemove = playlist.linkedFiches.map((lf) => {return lf._id.toString()}).indexOf(ficheToRemoveId);

      if (idToRemove > -1) {
        if (idToRemove > 0) {
          if (idToRemove < playlist.linkedFiches.length - 1) {
            playlist.linkedFiches[idToRemove - 1].next = playlist.linkedFiches[idToRemove].next;
          }
          else {
            playlist.linkedFiches[idToRemove - 1].next = null;
          }

          playlist.linkedFiches[idToRemove - 1].value = null;
          playlist.linkedFiches[idToRemove - 1].save();
        }

        playlist.linkedFiches.splice(idToRemove, 1);
      }

      playlist.save((err) => {
          if (err) {
            console.log(err);
            res.status(500).send("linkedFiche not created");
          }
          playlist.save((err, fiche) => {
            return prePopulate(req, res, playlist, false);
          });
      });
    });
  });
}

export function addLink(req, res) {
  Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
    if (err) {
        res.status(500).send("playlist not found");
    }

    Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
        populate: {
            path: 'fiche', model: 'Fiche'}
    }, function (err, playlist) {
      if (err) {
          res.status(401).send("Populating failed");
      }

      var newLinkedFiche = new LinkedFiche({
          fiche: req.body.fiche==undefined||req.body.fiche==null?req.body.ficheId:req.body.fiche._id,
          value: null,
          next: null
      });

      newLinkedFiche.save((err, fiche) => {
        if (err) {
            console.log(err);
            res.status(500).send("linkedFiche not created");
        }

        playlist.linkedFiches.push(fiche);

        if (playlist.linkedFiches.length - 1 > 0) {
          var linkedFiche = playlist.linkedFiches[playlist.linkedFiches.length - 2];
          linkedFiche.value = req.body.linkValue!==''?req.body.linkValue:null;
          linkedFiche.next = fiche._id;

          linkedFiche.save((err) => {
            if (err) {
              res.status(500).send("linkedFiche not created");
            }

            playlist.save((err, fiche) => {
              Playlist.populate(playlist, {
                  path: "linkedFiches", model: 'LinkedFiche',
                  populate: {
                    path: 'fiche', model: 'Fiche'}
              }, function (err, playlist) {
                if (err) {
                  res.status(401).send("Populating failed");
                }

                return prePopulate(req, res, playlist, false);
              });
            });
          })
        }
        else {
          playlist.save((err, fiche) => {
            Playlist.populate(playlist, {
              path: "linkedFiches", model: 'LinkedFiche',
              populate: {
                  path: 'fiche', model: 'Fiche'}
            }, function (err, playlist) {
              if (err) {
                res.status(400).send("Populating failed");
              }

              //fisrt fiche added to the playlist we take the fiche cover
              playlist.cover = {
                path: playlist.linkedFiches[0].fiche.cover.path
              };

              playlist.save();

              return prePopulate(req, res, playlist, false);
            });
          });
        }
      });
    });
  });
}

export function updateLink(req, res) {
  Playlist.findOne({ _id : req.params.id}, function (err, playlist) {
    if (err) {
      res.status(500).send("playlist not found");
    }

    Playlist.populate(playlist, {path: "linkedFiches", model: 'LinkedFiche',
        populate: {
            path: 'fiche', model: 'Fiche'}
    }, function (err, playlist) {
      if (err) {
        res.status(400).send("Populating failed");
      }

      var ficheToReplaceId = req.body.fiche==undefined||req.body.fiche==null?req.body.ficheId:req.body.fiche._id;

      var idToReplace = playlist.linkedFiches.map((lf) => {return lf.fiche._id.toString()}).indexOf(ficheToReplaceId);

      if (idToReplace === -1) {
        return res.status(500).send("cannot perform the updateLink, bad params");
      }

      playlist.linkedFiches[idToReplace].value = req.body.linkValue? req.body.linkValue: "";

      playlist.linkedFiches[idToReplace].save((err) => {
        if (err) {
          console.log(err);
          res.status(500).send("linkedFiche not created");
        }

        playlist.save((err, fiche) => {
          return prePopulate(req, res, playlist, false);
        });

      });
    });
  });
}

//just updating the status of the playlist, would be better to keep track of it, since now we are will handle the case of Draf, Published and Deleted
export function remove(req, res) {
  const query = { _id : req.params.id};

  Playlist.findOne(query, (err, playlist) => {
    if(err) {
      console.log("error " + err);
      return res.status(500).send('No playlist were found: '+ query._id);
    }

    Playlist.update(query, {status: CONSTANTS.STATUS.DELETED}, (err, previousOne) => {
      if(err) {
        console.log("error " + err);
        return res.status(500).send('We failed to delete the playlist : '+ query._id);
      }
      return res.status(200).send({message: "Playlist supprimée avec succès"});
    });
  });
}

export function update(req, res) {
  const query = {_id : req.params.id};

  Playlist.findOne( query, (err, playlist) => {
    if(err){
      return res.status(500).send('Failed to update the data of '+ query._id);
    }

    const { body } = req;

    playlist.name = body.name? body.name: "";
    playlist.description = body.description? body.description: "";

    playlist.save((err, playlist) => {
      return prePopulate(req, res, playlist, false);
    });
  });
}

export function fullUpdate(req, res) {
  const query = {_id : req.params.id};

  Playlist.findOne( query, (err, playlist) => {
    if(err){
      return res.status(500).send('Failed to update the data of '+ query._id);
    }

    const body = req.body;

    let toSeek = "/medium";
    let photoUrl = body.photoUrl;

    if (photoUrl.indexOf(toSeek) === 0) {
      photoUrl = photoUrl.substring(toSeek.length);
    }

    playlist.name = body.name;
    playlist.description = body.description;
    playlist.cover = { path: photoUrl };
    //assume good structure
    playlist.labels = body.labels;

    playlist.status = body.status ? parseInt(body.status): status.DRAFT;
    playlist.save((err, onlyNewFields) => {
      return prePopulate(req, res, playlist, false);
    });
  });
}

export function publishPlaylist(req, res) {
  return togglePlaylistStatus(req, res, status.PUBLISHED);
}

export function unPublishPlaylist(req, res) {
  return togglePlaylistStatus(req, res, status.DRAFT);
}

//must retain private function
function togglePlaylistStatus(req, res, status) {
  const query = {_id : req.params.id},
        newStatus = status;

  Playlist.findOne( query, (err, playlist) => {
    if(err){
        return res.status(500).send('Failed to update the data of '+ query._id);
    }
    playlist.name = req.body.name;
    playlist.description = req.body.description;
    playlist.status = newStatus;

    playlist.save((err, playlist) => {
      return prePopulate(req, res, playlist, false);
        /*Playlist.populate(playlist, {
            path: "linkedFiches", model: 'LinkedFiche',
            populate: {
                path: 'fiche', model: 'Fiche'}
        }, function (err, playlist) {
            if (err) {
                res.status(401).send("Populating failed");
            }

            applyModelVersion(Playlist, req, {populateLinkedFiches: true});

            return res.json(playlist);
        });*/
    });
  });
}




export function updateCover(req, res) {
    const query = {_id : req.params.id};

    Playlist.findOne( query, (err, playlist) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        playlist.cover = req.body;

        playlist.save((err, onlyNewFields) => {
          return prePopulate(req, res, playlist, false);
        });
    })
}

export function updateLabels(req, res) {
    const query = {_id : req.params.id},
          {body} = req;

    Playlist.findOne( query, (err, playlist) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        playlist.labels = {
          categories: body.categories? body.categories.map((id) => { return {_id: id} }): [],
          sousCategories: body.sousCategories? body.sousCategories: [],
          tags: body.tags? body.tags.map((id) => { return {_id: id} }): [],
        };

        playlist.save((err, fiche) => {

          if(err){
            return res.status(402).send('Failed to populate '+ query._id);
          }

          return prePopulate(req, res, playlist, false);
        });
    })
}


export function one(req, res, next, callback = null){
    const query = {_id : req.params.id};
    Playlist.findOne(query, (err, playlist)=> {
      if(err) {
          console.log('Error in first query');
          return res.status(500).send('Something went wrong getting the data');
      }

      return prePopulate(req, res, playlist, false, true, callback);
    })
}

export function stat(req, res) {
  return mixinStat(Playlist, req, res);
}

export function like(req, res) {
  return mixinLike(Playlist, req, res, (err, playlist) => {
    return prePopulate(req, res, playlist, false);
  });
}

export function dislike(req, res) {
  return mixinDislike(Playlist, req, res, (err, playlist) => {
    return prePopulate(req, res, playlist, false);
  });
}

function prePopulate(req, res, toPopulate, forList = false, hasToStringify = false, callback = null) {
  return Playlist.populate(toPopulate, [{
    path: "labels.categories", model: "Category"
  }, {
    path: "labels.tags", model: "Tag"
  }], (err, populated) => {
    if (err) {
      res.status(401).send("Populating failed");
    }

    return sendBackFullPlaylist(req, res, toPopulate, forList, hasToStringify, callback);
  });
}

function sendBackFullPlaylist(req, res, toSend, forList = false, hasToStringify = false, callback = null) {

  applyModelVersion(Playlist, req, {populateLinkedFiches: !forList, forList: forList});
  applyModelVersion(Fiche, req, {forList: forList});
  applyModelVersion(User, req, {forList: forList});

  if (callback != null) {
    return callback(hasToStringify? JSON.stringify(toSend): toSend);
  }else {
    return res.json(toSend);
  }
}

export default{
  all,
  add,
  one,
  addLink,
  updateLink,
  updateCover,
  updateLabels,
  swap,
  deleteFiche,
  remove,
  update,
  stat,
  publishPlaylist,
  unPublishPlaylist,
  fullUpdate,
  like,
  dislike
}
