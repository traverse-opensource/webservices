import Theme from '../models/themes';
import FicheController from '../controllers/fiches';
import { applyModelVersion, createDefaultTheme } from '../static';

import async from 'async';
import removeAccents from 'remove-accents';


export function all(req, res) {
    Theme.find({}).exec((err, themes) => {
        if (err) {
            console.log('Error in getting the themes -> function \'all\'');
            return res.status(500).send('Something went wrong getting the themes list');
        }
      
        applyModelVersion(Theme, req);
      
        return res.json(themes)
    })

}
export function one(req, res){
    const query = {_id: req.params.id};
    Theme.findOne( query, (err, theme) => {
        if (err) {
            console.log('Error cannot get the theme with the given id ' + query._id + ' -> function \'one\'');
            return res.status(500).send('Error cannot get the theme with the given id');
        }
      
        applyModelVersion(Theme, req);
      
        return res.json(theme);
    })
}

function retrieveDistinctThemeNames(callback) {
  Theme.distinct("name", (err, list) => {
    if (err) {
      return callback(err, null);
    }
    
    return callback(null, list.map((name) => {
      return removeAccents(name);
    }));
  });
}

function addNewThemesIfNecessary(req, res, receivedThemes) {
  return retrieveDistinctThemeNames((err, distinctNames) => {
    if (err) {
      console.log('@ThemeController > addNewThemesIfNecessary : ', err);
      return res.status(500).send('Error cannot add new themes');
    }
    
    const defaultTheme = createDefaultTheme();
    
    //getting only those which are not already in the databse
    let addingThemes = receivedThemes.filter((theme) => {
      return distinctNames.indexOf(removeAccents(theme.newName)) === -1;
    });
    
    //creating a new instance of theme for mongoose
    addingThemes = addingThemes.map((newThemeObject) => {
      return new Theme({
        name: newThemeObject.newName,
        color: newThemeObject.newColor ? newThemeObject.newColor: defaultTheme.color,
        description: defaultTheme.description
      });
    });
    
    let asyncThemeSave = addingThemes.map((theme) => {
      return (done) => {
        theme.save((err) => {
          if (err) {
            console.log('@ThemeController > updateDBThemes, adding new themes : ', err);
            return res.status(500).send('Error cannot update themes');
          }

          done();
        });
      };
    });

    async.series(asyncThemeSave, function allTaskCompleted() {
      console.log('@ThemeController > updateDBThemes, add done');

      return res.json(addingThemes);
    });
  });
}

function updateDBThemes(req, res, receivedThemes, localThemes) {
  
  const findIndexOfReceivingTheme = (receivedThemes, currentName) => {
    return receivedThemes.map((theme) => {
      return theme.oldName;
    }).indexOf(currentName);
  }
  
  localThemes = localThemes.map((dbTheme) => {
    
    let foundIndex = findIndexOfReceivingTheme(receivedThemes, dbTheme.name);
    
    if (foundIndex !== -1) {
      let newTheme = receivedThemes[foundIndex];
    
      dbTheme.name = newTheme.newName;

      if (newTheme.newColor) {
        dbTheme.color = newTheme.newColor;
      }
    }    
    
    return dbTheme;
  });
  
  
  let asyncThemeSave = localThemes.map((theme) => {
    return (done) => {
      theme.save((err) => {
        if (err) {
          console.log('@ThemeController > updateDBThemes, saving new name for old ones : ', err);
          return res.status(500).send('Error cannot update themes');
        }

        done();
      });
    };
  });
  
  async.series(asyncThemeSave, function allTaskCompleted() {
    console.log('@ThemeController > updateDBThemes, all done');
    return addNewThemesIfNecessary(req, res, receivedThemes);
  });
  //TODO aLso clean those who are in the list !
}

//be carefull, since this method will only be called by our swagger service with an extra header key
/*
 * This method aims to update all theme names by their respective names
 * The affected models would be => fiche and theme, since playlist have a theme based on its fiches
 *
 * 0) The idea is to find all theme where their name is equal to the one passed
 * 1) then, updated all the fiches that have this name
 * 2) finally, update all themes ith these new values
 */
export function updateThemeNames(req, res, next) {
  //TODO param to only update the fiches since some themes are dupplicates but written in another format
  const { themes } = req.body;
  
  Theme.find({}, (err, dbThemes) => {
    if (err) {
      console.log('@ThemeController > updateThemeNames, Cannot retrieve the actual themes : ', err);
      return res.status(500).send('Error cannot get themes');
    }
    
    //since we use slice in updateInnerTheme we need to make a copy of this array to maintain its values
    let receivingThemes = themes.map((theme) => {
      return theme;
    })
    
    return FicheController.updateInnerThemeByName(receivingThemes, dbThemes, (err, updateStatus) => {
      if (err) {
        console.log('@ThemeController > updateThemeNames, After fiche update callback : ', err);
        return res.status(500).send('Error cannot update themes');
      }
      
      console.log("after update inner themes", themes, receivingThemes);
      
      return updateDBThemes(req, res, themes, dbThemes);
    })
  });
}

export default{
    all,
    one,
    updateThemeNames
}
