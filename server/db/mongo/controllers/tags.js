import Tags from '../models/tags';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

//just for the force update
import Fiches from '../models/fiches';
import { applyModelVersion } from '../static';
import async from 'async';

/**
 * List
 */
export function all(req, res) {
    Tags.find({}).exec((err, tags) => {
        if (err) {
            console.log('Error in first query');
            return res.status(500).send('Something went wrong getting the data');
        }

        applyModelVersion(Tags, req);
        return res.json(tags);
    });
}

/*TODO maybe here we have to increase the count value
 here we have to
 - detect if the tag name exists
 - if not add a new one
 - otherwise, update this one
 */
export function add (req, res){
    let tags = new Tags({
        name: req.body.name,
    });
    tags.save((err, tags) =>{
        if(err){
            res.status(400).send("tags not added")
        }
        applyModelVersion(Tags, req);
        return res.json(tags);
    })
}

//TODO decrease count of the tag also
//from the console we never user this route, so this mean we don't and we shall not use the function fineOneAndRemove
//since it will remove it physically, just update the delete attribute or not, let's see together
export function remove (req, res){
    const query = { _id : req.params.id};
    console.log('TagController @remove> went here');
    Tags.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason');
        }
        return res.status(200).send("tags removed successfully");
    })

}
export function update(req, res) {
    const query = {_id : req.params.id};
    Tags.findOneAndUpdate( query, req.body, (err) => {
        if(err){
            return res.status(500).send('Failed to update the data of '+ query._id);
        }
        return res.status(200).send('tags updated');
    })
}

export function one(req, res){
    const query = {_id : req.params.id};
    Tags.findOne(query, (err, tags)=> {
        if(err) {
            return res.status(500).send('Something went wrong getting the data');
        }
        applyModelVersion(Tags, req);
        return res.json(tags);
    })
}

function bulkUpdate(tagsToUpdate, finalCallback){

  let callbacks = [];

  tagsToUpdate.forEach((toUpdate, index) => {
    callbacks.push((callback) => {
      Tags.update({'_id': toUpdate.id}, {'count': toUpdate.count}, (err, res) => {
        return callback(err, res);
      });
    })
  });

  async.parallel(
    callbacks,
    (err, results) => {
      //TODO sort the data by creation time
      if (err) {
        //return res.status(500).send("Cannot perform stats:" + err + ":" + results);
        return finalCallback(err, null);
      }

      return finalCallback(null, results);
      //return res.json({ data: results });
    });

    /*tagsToUpdate.forEach((toUpdate, index) => {
        Tags.update({'_id': toUpdate.id}, {'count': toUpdate.count}, (err, res) => {
            //console.log(err ? "Could't update tag for tagID:" + toUpdate.id : "Update successfully");
        });
    });*/
}

function countCallback (ids, i, auxArr, callback){
    /*console.log('#');
    console.log(ids);
    console.log('#');*/
    Fiches.count({tags: ids[i]}, (err, count) => {

      auxArr.push({id: ids[i], count: count});

      if (i < ids.length){
        return countCallback(ids, ++i, auxArr, callback);
      }else{
        return bulkUpdate(auxArr, callback);
      }
    });
}

//this controller call shall be disabled when not needed
function updateCount2(req, res){
  //recover all ids to loop through it
  const queryTagIds = "_id";
  Tags.distinct(queryTagIds, (err, ids) => {
    if(err) {
        return res.status(500).send('Cannot loop through all ids');
    }

    console.log(ids.length);

    let toUpdate = [];
    countCallback(ids, 0, toUpdate, (err, results) => {

      if (err) {
        console.log("@tagsController err in countCallback", err);
        res.status(500).send("Check the logs");
      }

      applyModelVersion(Tags, req);
      return res.json(ids);
    });
  });
}

function treatBadOnes(arrayOfBadTags, goodTag, upperCallback) {

  console.log("@tagsController goodTag", goodTag.name, goodTag.count, goodTag._id);

  let callbacks = arrayOfBadTags.map((tag) => {
    return (callback) => {
      return Fiches.find({tags: {$in:[tag._id]}}, (err, fichesToRelink) => {
        console.log("@tagsController goodTag", tag.name, tag.count, tag._id);
        //don't need to keep track of the fiche itself here
        fichesToRelink.forEach((fiche) => {

          //console.log("phoque", indexToUpdate, fiche.tags, tag._id, goodTag._id);
          fiche.tags = fiche.tags.map((tagToTest) => {
            if (tagToTest._id.toString() == tag._id.toString()) {
              tagToTest = goodTag;
            }
            return tagToTest;
          });

          //dont't care about the result, just redo it as many time as needed
          fiche.save();
        });
        return callback(null, tag._id);
      });
    }
  });

  async.series(
    callbacks,
    function(err, badOnes) {
      //removing the badIds now
      Tags.find({_id: badOnes}).remove((err) => {
        return upperCallback(err, goodTag);
      })
    }
  );
}

export function updateCount(req, res, next) {
  Tags.distinct("name", (err, names) => {
    if (err) {
      console.log("@TagsController cannot find distinct on names", err);
      res.status(500).send("Check the logs");
    }

    let callbacks = names.map((name) => {
      return (callback) => {
        return Tags.find({ name: name }, (err, tags) => {
          if (tags.length === 1) {
            //no need to update here since ales ist gut
            return callback(null, tags[0]);
          }else if (tags.length > 1) {
            let sorted = tags.sort((a, b) => { return a.count < b.count; })
            //sorted will only contain bad ones after being shifted
            let biggest = sorted.shift();

            return treatBadOnes(sorted, biggest, (err, results) => {
              console.log("------------------Where is my mongoose???-----------------")
              //updating count of the bigOne since it's shall be the good one
              Fiches.count({tags: {$in:[biggest._id]}}, (err, counted) => {

                biggest.count = counted;
                biggest.save((err) => {
                  return callback(err, biggest);
                })
              });
            });

          }else { // should never arrive here
            return callback("Should not have zero tags with a distinct name found previously!!", []);
          }
        })
      }
    });

    async.series(
      callbacks,
      function(err, results) {
        let toDebug = err? "what a failure error happens": results.map((result) => { return result.name + " " + result.count });
        return err? res.status(500).send(toDebug): res.json(toDebug)
        //return updateCount2(req, res);
      }
    );
  });
}

function replaceTagObjectId (req, res, toFix, realTagId, badId, helper){

  if (toFix.length > 0){
    let currentFiche = toFix.pop();

    //checking into Tags
    let replacePos = currentFiche.tags.indexOf(badId);
    currentFiche.tags[replacePos] = realTagId;

    let yolo = "tags." + replacePos;

    let updatequery = {tags: currentFiche.tags};

    Fiches.update({'_id': currentFiche._id}, updatequery, (err, res) => {

      if(err) {
        let response = 'Cannot update fiche with ficheId > ' + currentFiche._id;
        return res.status(500).send(response);
      }

      replaceTagObjectId (req, res, toFix, realTagId, badId, helper);
    });
  }
  else {
    fixTagsWithRealOne(helper[0], helper[1], helper[2], helper[3]);
  }

}

//only for one tag at a time
function fixTagsWithRealOne(req, res, badOnes, result = {fichesToRelink: [], tagsToDelete: []}) {
  console.log('>---', badOnes.length);
  if (badOnes.length > 0){
    //just removing the last item of the array
    let toDelete = badOnes.pop();
    let targetObjectId = toDelete._id;
    const query = {"tags": targetObjectId};

    Fiches.find(query, (err, toFix) => {
      if(err) {
        return res.status(500).send('Cannot get the fiches that are linked to bad ones');
      }

      console.log({
        tagObjectId: targetObjectId,
        linkedFiches: toFix
      });

      result.fichesToRelink.push(toFix);
      result.tagsToDelete.push(toDelete);

      //find the index of the bad one and replace it with the realId, so we need to find the real Object Id
      let regex = /\s\(\s\d+\s\)/g;
      let position = regex.exec(toDelete.name).index -1;
      let nameToFind = toDelete.name.substr(0, position);

      Tags.find({"name": nameToFind}, (err, realTags) => {
        if(err) {
          return res.status(500).send('Cannot get the real tag');
        }

        let realTag = realTags[0];

        let helper = [req, res, badOnes, result];

        /*
        console.log('<--------------');
        console.log(realTag);
        console.log('####');
        console.log(nameToFind);
        console.log(realTag.name, realTag._id);
        console.log(toDelete.name, toDelete._id);
        console.log('####');
        console.log('-------------->');
        */
        replaceTagObjectId(req, res, toFix, realTag._id, toDelete._id, helper);
      });

    });
  }
  else {
    //fix Count and remove bad ones, shall be in result.tagsToDelete


    removeTagsWithId(req, res, result.tagsToDelete);
    //return res.json(result);
  }

}

function removeTagsWithId(req, res, arrToRemove) {
  console.log("------removeTagsWithId", arrToRemove.length, "-------");
  if (arrToRemove.length > 0) {
    let toPop = arrToRemove.pop();
    let removeQuery = {_id: toPop._id};
    Tags.remove(removeQuery, (err, result) => {
      if(err) {
        let response = 'Cannot physycally delete tag with id > ' + toPop._id;
        return res.status(500).send(response);
      }

      removeTagsWithId(req, res, arrToRemove);
    });
  }else {

    let findNoName = {"name": null};

    Tags.find(findNoName, (err, result) => {

      if(err) {
        let response = 'Cannot find any empty names within Tags';
        return res.status(500).send(response);
      }

      if (result.length) {
        removeTagsWithId(req, res, result);
      }else {
        //update counts from here
        updateCount(req, res);
      }
    });
  }
}

//shall link all related fiche that contains tags that are like <tag name> ( number ) to the real one
//without the trailing count string and physically destroy those tags
//at the end of the process, we shall call also updateCount to fix all tag counts at once
export function purgeCount(req, res){
  const queryTagWeirdName = {"name": /\s\(\s\d+\s\)/g};
  Tags.find(queryTagWeirdName, (err, badOnes) => {

    if(err) {
        return res.status(500).send('Cannot get the list of the bad ones');
    }

    fixTagsWithRealOne(req, res, badOnes);
  });
}

export default{
    all,
    one,
    add,
    remove,
    update,
    updateCount,
    countCallback,
    purgeCount
}
