import Feed from '../models/feeds';
import User from '../models/users';
import UserController from '../controllers/users';
import Fiche from '../models/fiches';
import thumbnailService from '../../../services/thumbnail';
import async from 'async';
import { FEED_STATUS, FEED_STATUS_STRING, MODEL_LIMITS, RIGHTS, PAGINATION_DEFAULT, FEED_KINDS } from '../constant';
import { applyModelVersion, isFromEditorialPlatform, getUniqueStringsInArray, createSkipQuery } from '../static';

import multer from 'multer' ;
//after uploaded, need to scale down images

/**
 * List only for one fiche, shall take care also if the request comes from the editorial platform
 * We can have this information if we take a look at the mtsk, the one comes from editorial shall have all informations
 */
export function all(req, res, next, callback = null) {

  let ficheId = req.params.id;

  //creating query only if the fiche comes from mobile request
  let
    query = {
      fiche_id: ficheId,
      status: { $nin: [ FEED_STATUS.DELETED ] }
    },
    page = req.query.page? parseInt(req.query.page): PAGINATION_DEFAULT.PAGE,
    limit = req.query.limit? parseInt(req.query.limit): PAGINATION_DEFAULT.LIMIT_FEED;


  if (isFromEditorialPlatform(req)) {
    delete query.status;
  }

//check with editorial secret key
//TODO to remove when done with testing on editorial
console.log("feed controller", query, req.versionId);

  Feed.find(query).exec((err, feeds) => {
    if (err) {
      console.log('@FeedController, list by ficheId failed > err', err);
      if (callback != null) {
        return callback(err, null);
      }else {
        return res.status(500).send('Failed to retrieve feeds for ficheId ' + ficheId);
      }
    }

    if (callback != null) {
      return callback(null, feeds);
    }else {
      applyModelVersion(User, req, { forList: true });
      applyModelVersion(Fiche, req, { forList: true });
      applyModelVersion(Feed, req);

      let skip = createSkipQuery(page, limit);

      return res.json(feeds.slice(skip, skip + limit));
    }
  });
}

function generateThumbs(file, callback) {
  thumbnailService.imageThumb({
    baseDir: file.destination,
    fileName: file.filename,
    callback: (err, result) => {
      if (err) {
        console.log("@feedController: issue on rescaling > ", err);
        return callback(err, null);
      }

      let toSend = file;
      toSend.thumb = result.scaled;
      return callback(null, result);
    }
  });
}

export function uploadFeedPicture (req, res, next) {
  const query = {_id : req.params.id},
      id = req.params.id;

  const upload = multer({ dest: 'uploaded_images/feeds/' }).single('file');

  upload(req, res, function(err) {
    const { body } = req;

    let file = req.file;

    //need to generate thumbs
    return generateThumbs(file, (err, fileObj) => {
      if(err){
        return res.status(500).send('could not upload file '+ err);
      }

      User.findOne({ _id: body.created_by }, (err, user) => {
        if (err || user == null) {
          console.log("@FeedController error in getting user, err", err);
          return res.status(401).send('User does not exist');
        }

        req.body = {
          fiche_id: id,
          created_by: user,
          description: body.description,

          //Will transform json path on before send transformation
          cover: {
            path: req.file.path
          }
        };

        return add(req, res);
      });
    });
  });
}

/**
 * Add a Feed to a fiche
 */
export function add(req, res) {

  let ficheId = req.params.id;

  let feed = new Feed(req.body);

  feed.save((err) => {
    if (err) {
      console.log("@feed controller save new feed by user, err", err);
      return res.status(500).send("Could not save feed");
    }

    applyModelVersion(User, req);
    applyModelVersion(Fiche, req);
    applyModelVersion(Feed, req);

    return res.status(200).send(feed);
  });
}

function retrieveUserForAdminActions(userId, versionId, callback) {

  let
    fakeReq = { params: { id: userId, versionId: versionId }, versionId: versionId },
    fakeRes = {},
    fakeNext = {};

  return UserController.getUser(fakeReq, fakeRes, fakeNext, callback);
}

function handleBadRequests (req, res, next, callback) {
  if (isFromEditorialPlatform(req)) {
    return callback();
  }else {
    return res.status(401).send("Bad request");
  }
}

function alterFeedIfHasEnoughRights(req, res, next, targetStatus) {
  return handleBadRequests(req, res, next, () => {
    console.log(req.params)
    return retrieveUserForAdminActions(req.params.userId, req.versionId, (err, user) => {
      //encountered error, no user or there is user but no rights send back bad request
      if (err || !user || (user && !RIGHTS.COMPARE_GROUP(user.group))) {
        console.log("@feed controller > trying to update a feed, err", err, user);
        return res.status(401).send("Bad request");
      }
      return updateFeedStatus(req, res, next, targetStatus);
    });
  });
}

function updateFeedStatus(req, res, next, targetStatus) {
  let feedId = req.params.id;

  Feed.findOneAndUpdate({ _id: feedId }, { $set: { status: targetStatus } }, { new: true }, function(err, feed) {
    if (err || feed == null) {
      console.log("@feed controller > flag a feed, err", err);
      return res.status(500).send("Internal error");
    }

    applyModelVersion(User, req);
    applyModelVersion(Fiche, req);
    applyModelVersion(Feed, req);

    return res.status(200).send(feed);
  });
}

export function flag(req, res, next) {
  return updateFeedStatus(req, res, next, FEED_STATUS.FLAGGED);
}

//remove, post and approve are only accessible with editorial secretKey thus an admin nor super admin userid
export function remove(req, res, next) {
  return alterFeedIfHasEnoughRights(req, res, next, FEED_STATUS.DELETED);
}

export function post(req, res, next) {
  return alterFeedIfHasEnoughRights(req, res, next, FEED_STATUS.POSTED);
}

export function approve(req, res, next) {
  return alterFeedIfHasEnoughRights(req, res, next, FEED_STATUS.APPROVED);
}

function seekShortFiches(ficheIds, callback) {
  return Fiche.find({ _id: { $in: ficheIds } }, (err, fiches) => {
    if (err) {
      return callback(err, null);
    }

    const buildThumbPath = (fiche) => {
      //update cover
      let bigPath = fiche.cover.path,
          mediumPath = bigPath,
          thumbPath = bigPath;

      if (bigPath && bigPath.indexOf("http") === -1) {
        let splitted = bigPath.split("/");
        mediumPath = splitted[0] + "/medium/" + splitted[1];
        thumbPath  = splitted[0] + "/thumb/" + splitted[1];
      }

      return thumbPath;
    };

    let results = [];

    fiches.forEach((fiche) => {
      let ficheId = fiche._id.toString();
      results[ficheId] = {
        ficheId: {
          _id: ficheId,
          title: fiche.name,
          path: buildThumbPath(fiche),
          __t: fiche.__t
        }
      };
    });

    //send a hashmap identified by fiche id so easier to recover them in the resulting feed list
    return callback(null, results);
  });
}

//TODO pagination system for this query
function editorialFeed(req, res, next, query, order) {
  return handleBadRequests(req, res, next, () => {

    const page = req.query.page? parseInt(req.query.page): PAGINATION_DEFAULT.PAGE,
      limit = req.query.limit? parseInt(req.query.limit): PAGINATION_DEFAULT.LIMIT;

    //only seeking for feeds that comes from traverse since if it comes from social network, the feeds shall be moderated by the owner of the page/group/content
    query.kind = FEED_KINDS.TRAVERSE;

    return Feed.find(query).skip(createSkipQuery(page, limit)).limit(limit).sort(order).exec((err, feeds) => {
      if (err) {
        console.log("@FeedController recover treated, err", err);
        return res.status(500).send("Failed to perform request");
      }

      let fichesIds = feeds.map((feed) => {
        return feed.fiche_id._id.toString();
      });

      return seekShortFiches(getUniqueStringsInArray(fichesIds), (err, fiches) => {
        if (err) {
          console.log("@FeedController trated, cannot retrieve fiches, err", err);
          return res.status(500).send("Failed to perform request");
        }



        let toReturn = feeds.map((feed) => {
          return {
            fiche: fiches[feed.fiche_id._id].ficheId,
            feed: feed
          }
        });

        applyModelVersion(User, req);
        applyModelVersion(Fiche, req);
        applyModelVersion(Feed, req);

        return res.json(toReturn);
      })
    });
  });
}

export function deleted(req, res, next) {
  return editorialFeed(req, res, next, { status: FEED_STATUS.DELETED }, { created_at: -1 });
}
export function flagged(req, res, next) {
  return editorialFeed(req, res, next, { status: FEED_STATUS.FLAGGED }, { created_at: 1 });
}
export function posted(req, res, next) {
  return editorialFeed(req, res, next, { status: FEED_STATUS.POSTED }, { created_at: 1 });
}
export function approved(req, res, next) {
  return editorialFeed(req, res, next, { status: FEED_STATUS.APPROVED }, { created_at: -1 });
}

//********************************************************************************
//TODO add pagination system for these request, the pagination can be achieved directly here by the second part of the search query
export function treated(req, res, next) {
  return editorialFeed(req, res, next, { updated_at: { $exists: true}}, { created_at: -1 });
}

export function nonTreated(req, res, next) {
  return editorialFeed(req, res, next, { updated_at: { $exists: false}}, { created_at: -1 });
}
//********************************************************************************

function countByStatus(targetStatus, targetName, callback) {
  return Feed.count({ status: targetStatus }, (err, count) => {
    return err? callback(err, null): callback(null, { name: targetName, count: count });
  });
}

export function stats(req, res, next) {
  return handleBadRequests(req, res, next, () => {

    async.parallel([
      (callback) => {
        return countByStatus(FEED_STATUS.DELETED, FEED_STATUS_STRING.DELETED, (err, count) => { return callback(err, count) });
      },
      (callback) => {
        return countByStatus(FEED_STATUS.FLAGGED, FEED_STATUS_STRING.FLAGGED, (err, count) => { return callback(err, count) });
      },
      (callback) => {
        return countByStatus(FEED_STATUS.POSTED, FEED_STATUS_STRING.POSTED, (err, count) => { return callback(err, count) });
      },
      (callback) => {
        return countByStatus(FEED_STATUS.APPROVED, FEED_STATUS_STRING.APPROVED, (err, count) => { return callback(err, count) });
      }
    ],
    (err, results) => {
      //TODO sort the data by creation time
      if (err) {
        return res.status(500).send("Cannot perform stats:" + err + ":" + results);
      }

      return res.json({ data: results });
    });
  });
}

export default {
  all,
  uploadFeedPicture,
  flag,
  remove,
  post,
  approve,
  treated,
  nonTreated,
  stats,
  deleted,
  flagged,
  posted,
  approved
};
