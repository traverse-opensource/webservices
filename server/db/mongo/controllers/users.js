require('babel-polyfill');
import passport from 'passport';
import User from '../models/users';
import Fiche from '../models/fiches';
import Group from '../models/groups';
import Entity from '../models/entity';
import FicheController from './fiches';
import LinkedFiches from '../models/linkedFiches';
import Playlist from '../models/playlists';
import { permission, RIGHTS, status, PAGINATION_DEFAULT, LOCATION_DEFAULT } from '../constant';
import { createSkipQuery, sortByRankingAlgorithm, applyModelVersion, performArrayUpdateBasedOnModel } from '../static';
import Mailer from '../../../services/mailer';
import async from 'async';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import json2csv from 'json2csv';

import multer from 'multer' ;
const uploadUser = multer( { dest: 'uploaded_images/user_profiles/' } );

/**
 * POST /login
 */
export function login(req, res, next) {
  // Do email and password validation for the server
  passport.authenticate('local', (authErr, user, info) => {

    if (authErr) return next(authErr);
    if (!user) {
      return res.status(400).json({ message: info.message });
    }
    // Passport exposes a login() function on req (also aliased as
    // logIn()) that can be used to establish a login session

    if (user.group.title === permission.DEACTIVATED) {
      return res.status(401).json({
        message: 'Utilisateur supprimé'
      });
    } else {
      return req.logIn(user, (loginErr) => {
        if (loginErr) return res.status(401).json({ message: loginErr });

        applyModelVersion(User, req);

        return res.status(200).json(user);
      });
    }

  })(req, res, next);
}

export function forgotPassword(req,res,next){
  crypto.randomBytes(20, function(err, buf) {
    var token = buf.toString('hex');
    User.findOne({ email: req.body.email }, function(err, user) {
      if (!user) {
        return res.status(400).json({message: 'Pas de compte avec cet email'});
      }
      user.resetPasswordToken = token;
      user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
      user.save(function(err,user) {
        if(err)return  res.status(401).json({message: 'Problème avec la mise à jour du mot de passe.'});
        let smtpTransport = nodemailer.createTransport({
          host: 'mail.infomaniak.com',
          port: 587,
          secure: false, // secure:true for port 465, secure:false for port 587
          requireTLS: true,
          auth: {
            user: 'noreply@traverse-patrimoines.com',
            pass: 'noreply@2017$'
          }
        });
        let mailOptions = {
          from: 'Noreply <noreply@traverse-patrimoines.com>', // sender address
          to: user.email , // list of receivers
          subject: 'Réinitialiser le mot de passe Tarverse', // Subject line
          text: 'Vous recevez cet email car vous avez demandé de réinitialiser votre mot de passe de la plateforme TRAVERSE. \n\n'+
          'Merci de cliquer sur le lien ci dessous ou de le copier dans la barre d\'adresse de votre navigateur.\n\n'+
          'https://www.traverse-patrimoines.ch:8000/resetPassword/'+token+'\n\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
              if(!err) {
                applyModelVersion(User, req);
                return res.status(200).json({message: 'Un email a été envoyé à ' + user.email + ', merci de suivre les instructions.', user: user});
              }
              else{return res.status(500).json({message: 'impossible d\'envoyer un mail à ' + user.email + ' erreur : ' + err})}
            }
        );
      });
    });
  });
}
/**
 * POST /logout
 */
export function logout(req, res) {
  // Do email and password validation for the server
  //for now just return a custom message, since we don't track anything
        //good one before
        //req.logout();
        //res.redirect('/');
  res.send({message: "log out successfully"});
  /*req.session.destroy(function (err) {
   res.redirect('/'); //Inside a callback… bulletproof!
   });*/
}

//first check before trying to seek nor to create a new user, avoids some side effects
function checkUserSignUpParams(body) {

  let success = false;
  let message = "Some parameters are missing";

  if (body.kind) {
    //social network case
    success = true;
    if (!(body.email && body.name && body.userName && body.userId)) {
      success = false;
      message = "Missing parameters for social login";
    }
  } else {
    //test against normal sign up (manual one)
    success = true;
    if (!((body.email || body.email === '') && body.name && body.password)) {
      success = false;
      message = "Missing parameters for manual login";
    }
  }

  return {
    success: success,
    message: message
  }
}

/**
 * POST /signup
 * Create a new local account, in this case, we will attribute this user a mobile user role
 */
export function signUp(req, res, next) {

  const { body } = req;

  let firstCheck = checkUserSignUpParams(body);

  if (!firstCheck.success) {
    return res.status(500).json(firstCheck);
  }

  function createUser (){
    return Group.findOne({title: permission.NOMAD}, (err, nomadRole) => {

      if (err) {
        console.log(err);
        return res.status(500).json({ message: 'Group does not exist yet' });
      }

      if(body.picture && body.picture !== null && body.picture !== undefined) {
        var pic = body.picture;
      } else {
        var pic = '';
      }

      let user = new User({
        email: body.email,
        password: body.password,
        profile: {
          name: body.name,
          picture: pic
        },
        group: nomadRole
      });

      if (body.kind && body.userId && body.userName) {
        user = new User({
            email: body.email,
            password: body.password,
            profile: {
                name: body.name,
                picture: pic
            },
            group: nomadRole,
            social: {
              kind: body.kind,
              value: body.userId,
              userName: body.userName
            }
        });
      }

      return localAdd(req, res, next, user, 1, body.password);
    });
  }

  console.log('body before seek', body);

  let searchQuery = body.kind ?
      {$or: [{"social.value": body.userId, "social.kind": body.kind}, {"email": body.email}]}: {"email": body.email};

  return User.findOne(searchQuery, (err, user) => {
    if (err) {

    }

    if (user != null && body.kind) {
      applyModelVersion(User, req);
      return res.json(user);
    } else if (user != null && !body.kind) {
      return res.status(400).send('Email existant, veuillez vous loguer');
    }
    else {
      return createUser();
    }
  });
}

/**
 * mode is to distinguish between sign up process and creation process
 * 0 for signUp process
 * 1 for create process
 */
function localAdd(req, res, next, user, mode = 1, clearPassword = null){

  //TODO fetch group here if does not eixsts take the default one, then put it on the json response!

  //shall be id of the group
  let group = req.body.groupId;
  let entities = req.body.entitiesList;

  function retrieveDefaultGroup(callback) {
    return Group.findOne({title: permission.NOMAD}, (groupErr, groupRes) => {
      if (groupErr) {
        return res.status(500).json({ message: 'Group does not exist yet' });
      }

      return callback(groupRes);
    });
  }

  function seekGroup() {
    if (group != null) {
      return Group.findOne({_id: group}, (groupErr, groupRes) => {
        if (groupErr) {
          //means no group available so give the default one
          return retrieveDefaultGroup(seekEntities);
        }

        return seekEntities(groupRes);
      });
    }else {
      //no group so we need to give the default one which is contributeur
      return retrieveDefaultGroup(seekEntities);
    }
  };

  function seekEntities(group) {
    const query = {
      _id: {
        $in: entities
      }
    }
    Entity.find(query, (err, entitiesRes) => {
      if (err) {
        return res.status(404).json({ message: "Ces groupes n'existent pas" });
      }

      return innerAdd(group, entitiesRes);
    })
  }

  function innerAdd(group, entitiesRes) {

    let entitiesId = entitiesRes.map((entity) => {
      return entity._id;
    });

    User.findOne({ email: req.body.email }, (findErr, existingUser) => {
      if (existingUser) {
        return res.status(409).json({ message: 'Un compte avec cet email existe déjà!' });
      }
      if(user.profile.name=='' || user.email=='' || user.password=='') {
        return res.status(401).json({message: "Veuillez remplir tous les champs"});
      }

      console.log('@innerAdd > password', clearPassword);

      let newUser = new User({
        email: user.email,
        password: clearPassword,
        profile: {
          name: user.profile.name,
          picture: user.profile.picture
        },
        group: group._id,
        entities: entitiesId,
        social: user.social
      });

      return newUser.save(newUser, (saveErr) => {
        if (saveErr) return next(saveErr);

        if (mode) {
          return req.logIn(newUser, (loginErr) => {
            if (loginErr) return res.status(401).json({ message: loginErr });

            applyModelVersion(User, req);

            return res.status(200).json(newUser);
          });
        } else {

          Mailer.sendMail(res, {name: newUser.profile.name, mail: newUser.email, password: clearPassword });

          //need to fetch group here

          applyModelVersion(User, req);

          return res.status(200).json({
            message: "Un email a été envoyé à " + newUser.profile.name + ": " + newUser.email,
            user: {
              email: newUser.email,
              group: group,
              _id: newUser._id,
              profile: newUser.profile,
              entities: entitiesRes
            }
          });
        }
      });
    });
  }

  return seekGroup();
}

/**
 * POST /users/create
 * Will create a new account, but this route is only available for admins or super admin,
 * So we need to check if the user that is creating this user is part of the group admin or super admin
 * need also to make this route only available through local
 */
export function add(req, res, next) {
  const
      { body } = req,
      //could be good if we store this
      creator = body.creator,
      user = new User({
        email: body.email,
        password: body.password,
        profile: {name: body.name},
        group: body.groupId,
        entities: body.entitiesList
      });

  if (body.groupId == null) {
    return res.status(404).json({ message: "Le rôle est obligatoire" });
  }

  //check admin privileges
  User.findOne({ _id: req.body.creator }, (findErr, creatorUser) => {
    if (findErr) {
      console.log("#err in adding user @creator", findErr);
      return res.status(409).json({ message: 'Un compte avec cet email existe déjà!' });
    }
    if (!RIGHTS.HAS_ENOUGH_PRIVILEGES(creatorUser, null, creatorUser.group)) {
      return res.status(404).json({ message: "Vous n'avez pas les droits nécessaires pour effectuer cette requette" });
    }

    return localAdd(req, res, next, user, 0, body.password);
  });
}

export function getUser(req, res, next, callback = null) {
  User.findOne({_id: req.params.id}, (err, user) => {
    if (err) {
      console.log('Error in getting the user');
      if (callback != null) {
        return callback(err, null);
      }else {
        return res.status(500).send('Something went wrong getting the user');
      }
    }

    if (callback != null) {
      return callback(null, user);
    }else {
      applyModelVersion(User, req);
      return res.json(user);
    }
  })
}

export function getUserPlaylists(req, res) {
    Playlist.find({created_by: req.params.id, status: { $in: [status.DRAFT, status.PUBLISHED] }}, (err, playlists) => {
        if (err) {
            console.log('Error in getting the playlists');
            return res.status(500).send('Something went wrong getting the playlists');
        }

        applyModelVersion(Playlist, req);

        return res.json({playlists: playlists});
    })
}

export function getUsers(req, res) {

  let query = {},
      body = req.query,
      limit = null,
      skipQuery = null;

  if(Object.keys(body).length > 0) {

    if (body.page) {
      skipQuery = createSkipQuery(parseInt(body.page), parseInt(body.limit));
    }

    if (body.limit) {
      limit = parseInt(body.limit);
    }
  }

  let toExec = User.find(query)
      .populate({ // populate the field which is another mongo Object
        path:'linkedFiches', //name of the field
        model: 'LinkedFiche', // the object I would like to get
      })
      .sort({'profile.name': 1});

  if (skipQuery !== null) {
    toExec = toExec.skip(skipQuery);
  }


  toExec.limit(limit).exec((err, users ) => {
    if (err) {
      console.log('UserController @getUsers: ', err);
      return res.status(500).send('Something went wrong getting the data');
    }

    applyModelVersion(User, req, {fromAll: true});
    return res.json({users: users});
  });
}

//merging the result to that we have a full mixing list, not separated from json props
function mergeFicheAndPlaylist(objectHolder) {
  let toReturn = objectHolder.fiches;
  const { fiches, matchingFicheIds, playlists } = objectHolder;

  let indexes = [],
      //need to add right after the first fiche that holds it, will increment each time we push into indexes array
      jump = 1,
      currentPlaylist = 0,
      foundIndex = -1;

  //avoidig to recomputing its too many times
  const ficheIds = fiches.map((f) => f._id.toString());

  //previosu good one
  matchingFicheIds.forEach((match) => {

    foundIndex = ficheIds.indexOf(match.ficheId.toString());

    if (foundIndex > -1) {
      //no need this step
      toReturn.splice(foundIndex + (jump++), 0, playlists[currentPlaylist++]);
    }
  });

  objectHolder.indexes = indexes;

  return toReturn;
}

export function retrievePlaylistHolders(fichesToRetrieve, req, res, callback = null){

  console.log('begin super async');

  let toReturn = {
        fiches: fichesToRetrieve,
        playlists: [],
        matchingFicheIds: [],
        hashPlaylists: {}
      },
      stack = [];

  //console.log("@UserController > retrievePlaylistsHolder : length to seek", fichesToRetrieve.length);

  //let start, mid = [], end;

  //start = new Date();

  let ficheIds = fichesToRetrieve.map((fiche) => {
    return fiche._id.toString();
  });


  return LinkedFiches.find({fiche: {$in: ficheIds}}, (err, results) => {
    if (err) {
      console.log("@UserController > detect linkedFiches, err:", err);
      return res.status(200).send("Weird error");
    }

    //recovers linkedFiches that are targetting fiches that we seek
    let lfIDs = results.map((lf) => {
      return lf._id;
    });

    //finding playlists that contains the targetted fiches and are published
    return Playlist.find({"linkedFiches" : {$in: lfIDs}, status: status.PUBLISHED}, (err, playlists) => {
       if (err) {
        console.log("@UserController > detect playlists, err:", err);
        return res.status(200).send("Weird error");
      }

      let added = [];

      //recover for one playlist, the fiche associated to it
      let orderedPlaylists = ficheIds.map((ficheID) => {
        for (let i = 0, len = playlists.length, currentP = null; i < len; ++i) {
          currentP = playlists[i];
          let filteredLinkefFiches = currentP.linkedFiches.filter((lf) => { return lf.fiche._id.toString() == ficheID });
          let isContainingFiche = filteredLinkefFiches.length > 0;
          if (added.indexOf(currentP._id.toString()) === -1 && isContainingFiche) {
            added.push(currentP._id.toString());
            toReturn.matchingFicheIds.push({ficheId: filteredLinkefFiches[0].fiche._id.toString(), playId: currentP._id.toString()})
            return currentP;
          }
        }
      }).filter((p) => {
        return p != null;
      });

      playlists = orderedPlaylists;

      applyModelVersion(User, req, {fromAll: true});

      //replaces the current playlists fetch from db and ensures unique playlists
      toReturn.playlists = playlists.filter((obj, pos, arr) => {
          return arr.map(mapObj => mapObj._id).indexOf(obj._id) === pos;
      });

      let finalOne = callback !== null?
        callback(mergeFicheAndPlaylist(toReturn)):
        res.json(mergeFicheAndPlaylist(toReturn));
    });
  });
}

function retrieveClosestItems(req, res, params) {

  const {nearQuery, max, filters} = params;

  let toExec = Fiche.find({
    location: {
      '$near': {
        '$maxDistance': max,
        '$geometry': nearQuery
      }
    },
    status: status.PUBLISHED,
    __t: { $in: filters}
  });

  //no need to skip here, we will do it from the latest result
  return toExec.exec(function(err, fiches) {

    if (err) {
      let errMessage = {err: err, debug: 'yolo'};
      return res.status(500).send('Something went wrong getting the playlists '+ JSON.stringify(errMessage));
    }

    Fiche.populate(fiches, [{
      path: 'categories',
      model: 'Category'
    },{
      path: 'tags',
      model: 'Tag'
    },{
      path: 'created_by',
      model: 'User'
    }, {
      path: 'related_fiches',
      model: 'Fiche'
    }], (err, innerResult) => {
      if (err) {
        return res.status(500).send('Something went wrong getting populate info'+ JSON.stringify(err));
      }

      return sortByRankingAlgorithm(
        innerResult,
        {
          latitude: params.lat,
          longitude: params.lng
        },
        (sortedFiches) => {
          return retrievePlaylistHolders(sortedFiches, req, res, (mixinList) => {
            let limit = params.limit,
                page = params.page;
            let first = createSkipQuery(page, limit);
            let toReturn = mixinList.slice(first, first + limit);

            applyModelVersion(User, req, {fromAll: true});
            applyModelVersion(Fiche, req, {forList: true});
            applyModelVersion(Playlist, req, {forList: true});

            return res.json(toReturn);
          });
        });
    });
  });
}

function getCreatedPlaylists(req, res, params, aux) {
  Playlist.count({created_by: params.userId, status: params.status}, (err, count) => {
    if (err) {
      return res.status(500).send('Something went wrong getting the created fiches> ' + params.status);
    }

    if (params.status === status.PUBLISHED) {
      aux.playlists.published = count;
      params.status = status.DRAFT;
      return getCreatedPlaylists(req, res, params, aux);
    } else if (params.status === status.DRAFT) {
      aux.playlists.draft = count;

      applyModelVersion(Playlist, req);

      return res.json(aux);
    }
    // don't handle deleted fiche nor playlists for now
  });
}

function getCreatedFiches(req, res, params, aux = {fiches: {draft: 0, published: 0}, playlists: {draft: 0, published: 0}}) {
  Fiche.count({created_by: params.userId, status: params.status}, (err, count) => {
    if (err) {
      return res.status(500).send('Something went wrong getting the created fiches> ' + params.status);
    }

    if (params.status === status.PUBLISHED) {
      aux.fiches.published = count;
      params.status = status.DRAFT;
      return getCreatedFiches(req, res, params, aux);
    } else if (params.status === status.DRAFT) {
      aux.fiches.draft = count;
      params.status = status.PUBLISHED;
      return getCreatedPlaylists(req, res, params, aux);
    }
    // don't need to handle deleted fiche nor playlists
  });
}

export function getCreatedItemCount(req, res) {
  let userId = req.params.id;
  User.findOne({_id: req.params.id}, (err, user) => {
    if (err) {
      return res.status(500).send('Something went wrong getting the user');
    }

    return getCreatedFiches(req, res, {userId: userId, status: status.PUBLISHED});
  })
}


//shall be geolocalized from the params
//shall also return the playlists that contains the fiches...
//carefull need to change the curl params and also the way to retrieve info, not using body in get params
function getSuggestions(req, res){

  /*console.log(req.query);
   console.log(req.params);
   console.log(req.body);*/

  let params = req.params,
      {query} = req,
      maxDistance = query.maxDistance ? query.maxDistance: LOCATION_DEFAULT.MAX_DISTANCE,
      lat = query.lat ? query.lat: LOCATION_DEFAULT.LAT,
      lng = query.lng ? query.lng: LOCATION_DEFAULT.LNG,
      //by default it's only places filters, if we filter with other kind of fiches we just need to request the same url but with a different array of filter with must match
      // fiche __t
      filters = query.filters ? query.filters: ["Places","People","Media","Objects","Events"],
      userId = params ? params.id: null,

      //pagination
      page = query.page ? parseInt(query.page): PAGINATION_DEFAULT.PAGE,
      limit = query.limit? parseInt(query.limit): PAGINATION_DEFAULT.LIMIT,

      //more convenient to pass params to other methods
      extraParams = {
        max: maxDistance,
        filters: filters,
        page: page,
        limit: limit,
        lat: lat,
        lng: lng
      };
  //distances are expressed in meters, are equivalent to the radius of the search and the user geopoint is the center

  if (userId) {
    User.findOne({_id: userId} , (err, user) => {
      if (err) {
        return res.status(500).send('Something went wrong getting the users '+ JSON.stringify(err));
      }

      extraParams.nearQuery = user.profile.lastKnownLocation;

      return retrieveClosestItems(req, res, extraParams);
    });


  }else {
    extraParams.nearQuery = {type: "Point", coordinates: [lng, lat]};

    //to make the geolocation search to work with mongo, we need to set the coordinates as the below query, which is first longitude and then latitude
    return retrieveClosestItems(req, res, extraParams);
  }
}
export function remove(req, res){
  const query = {_id : req.params.id}
  User.findOneAndRemove(query, (err) =>{
    if(err){
      console.log("error deleting the user with this id "+ req.params.id);
      return res.status(500).send('Failed to delete the user with this id '+ req.params.id);
    }

    applyModelVersion(User, req);

    return res.status(200).send('User with the id ' + req.params.id + " has been deleted from the database")
  })
}
/**
 * Checks wether the user who perform the action is super admin and perform the restore and deactivate user based on hasToRestore argument
 */
function toggleUserActivated(req, res, hasToRestore) {
  let { body } = req,
      superAdminId = body.superAdmin,
      userToUpdate = req.params.id;

  User.findOne({_id: superAdminId}, (err, superAdmin) => {
    if (err) {
      console.log("Step 0: Cannot " + (hasToRestore ? "restore": "deactivate") + " user with userId> " + userToUpdate + " " + err);
      return res.status(500).send("L'opération demandée ne peut pas s'effectuer");
    }

    if (err) {
      console.log("Step 1: Cannot " + (hasToRestore ? "restore": "deactivate") + " user with userId> " + userToUpdate + " " + err);
      return res.status(500).send("L'opération demandée ne peut pas s'effectuer");
    }

    //checks if super admin
    if (superAdmin.group.title === permission.SUPER_ADMIN) {
      User.findOne({_id: userToUpdate}, (err, user) => {
        if (err) {
          console.log("Step 2: Cannot " + (hasToRestore ? "restore": "deactivate") + " user with userId> " + userToUpdate + " " + err);
          return res.status(500).send("L'opération demandée ne peut pas s'effectuer");
        }


        //find the group base on the hasToRestore flag
        Group.findOne({title: hasToRestore ? permission.CONTRIBUTEUR: permission.DEACTIVATED}, (err, group) => {
          if (err) {
            console.log("Step 3: Cannot " + (hasToRestore ? "restore": "deactivate") + " user with userId> " + userToUpdate + " " + err);
            return res.status(500).send("L'opération demandée ne peut pas s'effectuer");
          }

          //update req.body to add the found group id based on if we have to restore or delete it
          req.body.group = group._id;

          return updateGroup(req, res, userToUpdate);
        });
      });
    }else {
      console.log("User: a non super admin tries to " + (hasToRestore ? "restore": "deactivate") + " user with userId> " + userToUpdate);
      return res.status(500).send("L'opération demandée ne peut pas s'effectuer");
    }
  });
}

export function deactivate(req, res) {
  return toggleUserActivated(req, res, false);
}

export function restore(req, res) {
  return toggleUserActivated(req, res, true);
}

function createUpdateParams(body, oldUser) {
  let toReturn = {
    email: oldUser.email,
    password: oldUser.password,
    group: oldUser.group,
    profile: oldUser.profile,
    entities: oldUser.entities
  };

  function updateJSONParams(key, parent = null) {

    let isProfile = parent != null,
        isIncluded = false;

    if (isProfile) {
      if (body[parent]){
        isIncluded = body[parent][key];
      }
    }

    if (body[key] || isIncluded) {

      if (isProfile) {
        //avoiding overriding profile if already set
        let profile = toReturn.profile ?
          toReturn.profile:
          {};
        profile[key] = body.profile[key];
        toReturn.profile = profile;
      }else {
        toReturn[key] = body[key];
      }
    }
  }

  //need to merge new user data with old user data so that extra data coming from clients won't be pushed into database
  updateJSONParams("password");
  updateJSONParams("email");
  updateJSONParams("picture", "profile");
  updateJSONParams("website", "profile");
  updateJSONParams("gender", "profile");
  updateJSONParams("name", "profile");

  return toReturn;
}

//need to do other way when referencing foreign objectid
/**
 * groupId is a string here that comes from front end, so we must ensure this ObjectId exists
 */
function updateGroup(req, res, id, updateParams = {}) {
  let searchQuery = {_id: req.body.group};
  return Group.findOne(searchQuery, (err, foundGroup) => {
    if(err){
      return res.status(500).send('Group does not exist '+ err);
    }

    updateParams.group = foundGroup;

    return localUpdate(req, res, id, updateParams);
  });
}

function updateEntities(req, res, id, updateParams) {
  return Entity.find({_id: {$in: req.body.entities}}, (err, foundEntities) => {
    if(err){
      return res.status(500).send('Entities does not exist '+ err);
    }

    updateParams.entities = foundEntities;

    return localUpdate(req, res, id, updateParams);
  });
}

//must not be called from outside
function localUpdate(req, res, id, updateParams) {

  //in order to update password in pre hook
  User.findOne({_id: id}, (err, toSend) => {

    if (updateParams.email)
      toSend.email = updateParams.email;
    /*if (updateParams.group)
      toSend.group = updateParams.group;
    if (updateParams.entities)
      toSend.entities = updateParams.entities;*/
    if (updateParams.password)
      toSend.password = updateParams.password;
    if (updateParams.profile)
      toSend.profile = updateParams.profile;

    toSend.save((err, toSend, numAffected) => {
      //err already tested twice
      //seems autopopulate does not work on update resquests

      applyModelVersion(User, req);

      if(err){
        console.log("UserController @localupdate > " + err);
        return res.status(500).send('Updating ' + id + ' failed');
      }

      return res.status(200).send({
        message: 'User with the id ' + toSend._id + " has been updated",
        user: toSend
      });
    });
  });
}

export function update(req, res) {
  const query = {_id : req.params.id},
      id = req.params.id,
      { body } = req;

  User.findOne(query, (err, user) => {
    if(err){
      return res.status(500).send('User does not exist '+ req.params.id);
    }

    let updateParams = createUpdateParams(body, user);

    /*if (body.group) {
      return updateGroup(req, res, id, updateParams);
    } else if (body.entities) {
      return updateEntities(req, res, id, updateParams);
    }else {*/
      return localUpdate(req, res, id, updateParams);
    //}
  });
}

export function comparePassword(req, res) {

  const query = {_id : req.params.id},
      id = req.params.id,
      { body } = req,
      { pass } = body;

  User.findOne(query, (err, user) => {
    if(err){
      return res.status(500).send('User does not exist '+ req.params.id);
    }

    // test a matching password
    user.comparePassword(pass, function(err, isMatch) {

      if(err){
        return res.status(500).send('Strange things happens '+ req.params.id);
      }

      console.log(pass, isMatch); // -&gt; Password123: true

      applyModelVersion(User, req);

      return res.status(200).send({
        message: "Do we have a match ?",
        match: isMatch
      });
    });
  });
}

export function updateProfilePicture (req, res, next) {
  const query = {_id : req.params.id},
      id = req.params.id,
      { body } = req;

  var upload = multer({ dest: 'uploaded_images/user_profiles/' }).single('file')
  upload(req, res, function(err) {

    if(err){
      return res.status(500).send('could not upload file '+ err);
    }

    req.body = { profile: { picture: req.file.path } }

    update(req, res);
  })
}
//not used in mobile request
export function getReset(req, res){
  User.findOne({resetPasswordToken:req.params.token, resetPasswordExpires:{$gt:Date.now()}},(err, user) => {
    if(!user) return res.status(500).json({message: 'Demande invalide ou expirée lol'});
    //res.redirect("https://localhost:3000/reset/"+req.params.token);

    applyModelVersion(User, req);

    return res.status(200).send({user})
  }).catch((err) => {console.log(err);return res.json({message : err})})
}

export function doReset(req, res){
  User.findOne({resetPasswordToken:req.params.token ,  resetPasswordExpires: { $gt: Date.now()}},
      (err, user) =>
      {
        if(!user){
          return res.status(402).json({message : "Demande invalide ou expirée"})
        }
        if(req.body.newPassword!= req.body.confirmPass){
          return res.status(402).json({message : "Les mots de passe ne correspondent pas."})
        }
        user.password = req.body.newPassword;
        user.resetPasswordToken= null;
        user.resetPasswordExpires = null;
        user.save((err, user) =>
        {
          if(err) return res.status(500).json({message: "impossible de changer le mot de passe " + err});
          let smtpTransport = nodemailer.createTransport({
            host: 'mail.infomaniak.com',
            port: 587,
            secure: false, // secure:true for port 465, secure:false for port 587
            requireTLS: true,
            auth: {
              user: 'noreply@traverse-patrimoines.com',
              pass: 'noreply@2017$'
            }
          });
          let mailOptions = {
            from: 'Noreply <noreply@traverse-patrimoines.com>', // sender address
            to: user.email , // list of receivers
            subject: 'Réinitialisation réussie', // Subject line
            text: 'La réinitialisation de votre mot de passe a bien été effectuée. \n\n'+
            'Vous pouvez maintenant vous connecter sur la plateforme TRAVERE avec votre nouveau mot de passe.\n\n'+
            'https://www.traverse-patrimoines.ch:8000/'
          };
          smtpTransport.sendMail(mailOptions, function(err) {
            if(!err) {
              applyModelVersion(User, req);
              return res.status(200).json({message: 'Email de confirmation envoyé', user: user});
            }
            else{return res.status(500).json({message: 'impossible d\'envoyer un mail à ' + user.email + ' erreur : ' + err})}
          })

          applyModelVersion(User, req);

          return res.status(200).json({message: "mot de passe changé avec succès" ,user:user})
        })
      })
}

export function downloadCSV(req, res) {
  let users = req.body.users;
  var fields = ['id', 'name', 'email'];

  try {
    var result = json2csv({ data: users, fields: fields });

    res.setHeader('Content-disposition', 'attachment; filename=testing.csv');
    //res.writeHead(200, { 'Content-Type': 'text/csv' });
    res.send(result);
  } catch (err) {
    // Errors are thrown for bad options, or if the data is empty and no fields are provided.
    // Be sure to provide fields if it is possible that your data array will be empty.
    console.log("UserController @downloadCSV > error: ", err);
    res.status(500).json({message: "Cannot transform data to csv"});
  }
}

export function getFavorites(req, res) {
  return User.findOne({_id : req.params.id}, (err, user) => {
    if (err) {
      console.log("@UserController > getFavorites >", err);
      res.status(400).send("An error occured");
    }

    const { favorites } = user;

    return Playlist.find({_id: { $in: favorites }}, (err, playlists) => {
      if (err) {
        console.log("@UserController > getFavorites >", err);
        res.status(401).send("An error occured");
      }

      applyModelVersion(Playlist, req, {forList: true});
      applyModelVersion(Fiche, req, {forList: true});
      applyModelVersion(User, req, {forList: true});

      return res.status(200).json(playlists);
    });
  });
}

export function syncFavorites(req, res) {
  return performArrayUpdateBasedOnModel(User, req, res, (user, playlistIds) => {
    //only adds a new like if not already in list
    if (!playlistIds) {
      playlistIds = [];
    }
    playlistIds.forEach((playlistId) => {
      if (user.favorites.indexOf(playlistId) === - 1) {
        user.favorites.push(playlistId);
      }
    });

    user.save((err, newFields) => {
      return res.json(user);
    });
  });
}

export function addFavorite(req, res) {
  return performArrayUpdateBasedOnModel(User, req, res, (user, playlistId) => {
    //only adds a new like if not already in list
    if (user.favorites.indexOf(playlistId) === - 1) {
      user.favorites.push(playlistId);
    }

    user.save((err, newFields) => {
      return res.json(user);
    });
  });
}

export function removeFavorite(req, res) {
  return performArrayUpdateBasedOnModel(User, req, res, (user, playlistId) => {
    //only removes a like if already in list
    let index = user.favorites.indexOf(playlistId);
    if (index !== -1) {
        user.favorites.splice(index, 1);
    }

    user.save((err, newFields) => {
      return res.json(user);
    });
  });
}

export default {
  login,
  logout,
  signUp,
  forgotPassword,
  add,
  getUser,
  getUserPlaylists,
  getUsers,
  getSuggestions,
  remove,
  deactivate,
  restore,
  update,
  getCreatedItemCount,
  comparePassword,
  updateProfilePicture,
  retrievePlaylistHolders,
  getReset,
  doReset,
  downloadCSV,
  getFavorites,
  addFavorite,
  removeFavorite,
  syncFavorites
};
