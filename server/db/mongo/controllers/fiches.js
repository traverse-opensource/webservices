/**
 * Created by hasj on 21/11/2016.
 */
//TODO carefull, the models are mispelled, can lead to turn around with a bug that should not happen -_-
import Fiche from "../models/fiches.js";
import Event from "../models/events.js";
import Media from "../models/media.js";
import Objet from "../models/objects.js";
import Places from "../models/places.js";
import People from "../models/people.js";
import User from "../models/users.js";
import Category from "../models/categories.js";
import LinkedFiche from "../models/linkedFiches";
import Playlist from "../models/playlists";
import TagController from './tags';

//TODO check this side effect
import { createSkipQuery, FICHE_TYPES, status, FICHE_FILTERS, applyModelVersion, mixinStat, mixinLike, mixinDislike } from '../static';

//TODO why this import ???
//import mongoose from 'mongoose';
import async from 'async';

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

/**
 * List
 */

let has = (object, key) => {
  return object.hasOwnProperty(key);
  //return object ? hasOwnProperty.call(object, key) : false;
}

//instead, this shall be done within the model and not this part of the process
let forceDefaultValues = (fiches) => {
  fiches.map((fiche) => {
    if (!fiche.social) {
      fiche.social = [];
    }
    if (!has(fiche.social, "facebook")) {
      fiche.social.facebook = {
        link: "",
        tags: []
      };
    }
    if (!has(fiche.social, "twitter")) {
      fiche.social.twitter = {
        link: "",
        tags: []
      };
    }
    if (!has(fiche.social, "instagram")) {
      fiche.social.instagram = {
        link: "",
        tags: []
      };
    }
    if (!has(fiche.social, "web")) {
      fiche.social.web = {
        link: ""
      };
    }
    //weird test
    if (fiche.social.length === 0) {
      fiche.social = {
        web: {link: ""},
        facebook: {tags: [], link: ""},
        twitter: {tags: [], link: ""},
        instagram: {tags: [], link: ""}
      };
    }
    return fiche;
  });

  return fiches;
};

const HASH_TYPES = [
  "Event",
  "Media",
  "Objet",
  "People",
  "Places"
]

export function all(req, res) {

    let query = {},
        body = req.query,
        limit = null,
        skipQuery = null,
        state = status.PUBLISHED,
        userId = null,
        filter = FICHE_FILTERS.ALL,
        searchQuery = "";

  /*  console.log(req.query);
  console.log(req.body);
  console.log(req.params);*/

    if(Object.keys(body).length > 0) {

      if (body.page) {
        skipQuery = createSkipQuery(parseInt(body.page), parseInt(body.limit));
      }

      if (body.limit) {
        limit = parseInt(body.limit);
      }

      if (body.userId && body.filter) {
        let filter = parseInt(body.filter);
        switch(filter) {
            case FICHE_FILTERS.ALL_DRAFT:
              query.status = status.DRAFT;
              break;
            case FICHE_FILTERS.MY_PUBLISHED:
              query.status = status.PUBLISHED;
              query.created_by = body.userId;
              break;
            case FICHE_FILTERS.MY_DRAFT:
              query.status = status.DRAFT;
              query.created_by = body.userId;
              break;
            //ALL filters
            default:
              query.status = status.PUBLISHED;
              break;
        }
      }

      if (body.filters) {
        //filters must be sent!
        let filters = body.filters;
        let searchQuery = {$or: []};
        let typeQuery = {__t: {$in: filters}};
        let or = [];

        let q = body.query;

        filters.filter((f) => {
          return f !== '';
        }).map((f) => {
          //recovering filters query that mongo understand
          return createSearchQuery(q, HASH_TYPES.indexOf(f), true);
        }).forEach((filterQuery) => {
          //transform into a flat one
          or = [...or, ...filterQuery];
        });

        searchQuery.$or = or;

        let andQuery = [searchQuery, typeQuery];
            //Object.assign({}, searchQuery, typeQuery);

        //query = Object.assign({}, query, searchQuery);
        query = Object.assign({}, query, (q != '' && q != null ? {$and: andQuery}: typeQuery));
      }


      //if no user ID no need to show custom filters

      //checking user id privileges also
      //checking pagination

    }

    let toExec = Fiche.find(query).populate([{
        path: 'categories',
        model: 'Category'
    },{
        path: 'tags',
        model: 'Tag'
    },{
        path: 'created_by',
        model: 'User'
    }]);

    if (skipQuery !== null) {
      toExec = toExec.skip(skipQuery);
    }

    toExec.limit(limit).sort({created_at: -1}).exec((err, fiches) => {
        if (err) {
            console.log('Error in first query @FicheController.all', err);
            return res.status(500).send('Something went wrong getting the data' + err);
        }

        let toReturn = forceDefaultValues(fiches);

        applyModelVersion(Fiche, req);

        if (limit){
          return res.json({fiches: toReturn});
        }else {
          return res.json(toReturn);
        }
    });
}

export function add (req, res){
    let fiche = new Fiche({
        name: req.body.name,
        categories: req.body.categories
    });

    fiche.save((err, fiche) =>{
        if(err){
            res.status(400).send("Fiche not added",err)
        }

        applyModelVersion(Fiche, req);
        return res.json(fiche);
    })
}

export function remove (req, res){
    const query = { _id : req.params.id};
    Fiche.findOneAndRemove( query, (err) =>{
        if(err) {
            console.log("error " + err);
            return res.status(500).send('We failed to delete for some reason',err);
        }
        return res.status(200).send("fiche removed successfully");
    })

}

export function updateGeoPositionIndexIfNeeded(req) {
  if (req.body.latitude != null && req.body.longitude != null) {
    req.body.location = {
      type : "Point",
      coordinates : [
        req.body.longitude,
        req.body.latitude
      ]
    };
  }
}

function recoverFichModel(ficheOpt = 0) {

  switch(ficheOpt){
    case 0: return Event;
    case 1: return Media;
    case 2: return Objet;
    case 3: return People;
    case 4: return Places;
  }

  //shall never reach this part
  return Fiche;
}

//selfClass cannot be null
export function update(req, res, opt = 0) {

  const query = {_id : req.params.id},
        me = this;

  updateGeoPositionIndexIfNeeded(req);

  let FicheModel = recoverFichModel(opt);

  FicheModel.findOneAndUpdate( query, req.body, {new: true}, function(err, updatedFiche){
    if(err){
      return res.status(500).send('Failed to update the data of '+ query._id);
    }

    updateTagsIfNeeded(updatedFiche, req, res);

    return searchForCreator(req, res, updatedFiche, updatedFiche.created_by);

    //return res.status(200).send(updatedFiche);
  });
}

function createSearchQuery(q, ficheOpt = 0, isMultiAtOne = false) {

  let toReturn = {};
  let toAppend = [];

  let or = [{name: new RegExp(q, "i")}];

  switch(ficheOpt) {
    case 0: toAppend = [{presentation: new RegExp(q, "i")}];
    case 1: toAppend = [{description: new RegExp(q, "i")}, {sousCategories: new RegExp(q, "i")}];
    case 2: toAppend = [{short_description: new RegExp(q, "i")}];
    case 3: toAppend = [{surname: new RegExp(q, "i")}, {short_bio: new RegExp(q, "i")}];
    case 4: toAppend = [{short_description: new RegExp(q, "i")}, {sousCategories: new RegExp(q, "i")}];
  }

  or = [...or, ...toAppend];

  if (!isMultiAtOne){
    toReturn.$or = or;
  }else {
    toReturn = or;
  }


  return toReturn;
}

export function search(req, res, opt = 0) {

  /*console.log(req.body);
  console.log(req.params);
  console.log(req.query);*/

  //TODO need to find the query params

  let FicheModel = recoverFichModel(opt);
  let query = createSearchQuery(req.query.q, opt);

  FicheModel.find(query, (err, fiches)=> {
    if(err) {
      if (Object.keys(err) > 0) {
        console.log('FicheController @genericSearch > err in first Query: ', err);
        return res.status(500).send('Something went wrong getting the data');
      }
    }
    let notDeletedFiches = [];
    fiches.map((fiche) => {
      //just sending published fiche here
      if(fiche.status === status.PUBLISHED){
        notDeletedFiches.push(fiche);
      }
    });

    applyModelVersion(Fiche, req);

    return res.json(notDeletedFiches);
  })
}

/**
  There is no need to send a non published fiche on mobile devices
 */
export function one(req, res, next, callback = null){
    const query = { _id : req.params.id, status: status.PUBLISHED };

    Fiche
      .findOne(query)
      .populate([
        { path: "related_fiches", model: "Fiche" }
      ])
      .exec((err, fiche) => {
        if(err) {
          console.log('Error in first query @FicheController.one', err);
          return res.status(500).send('Something went wrong getting the data');
        }

        applyModelVersion(Fiche, req, {forList: false});

        if (callback != null) {
          return callback(JSON.stringify(fiche));
        }else {
          return res.json(fiche);
        }
      });
}

export function addRelation(req, res) {

    Fiche.findOne({ _id : req.params.id}, function (err, fiche) {
        if (err) {
            res.status(500).send("fiche not found",err);
        }

        Fiche.findOne({ _id: req.body.fiche._id}, function (err, ficheToAdd) {

            if (fiche._id != ficheToAdd._id &&
                fiche.related_fiches.indexOf(ficheToAdd._id) == -1) {
                fiche.related_fiches.push(ficheToAdd);
                ficheToAdd.related_fiches.push(fiche);

            }

            fiche.save((err, fiche) => {
                ficheToAdd.save((err, ficheToAdd) => {

                    Fiche.populate(fiche, {path: "related_fiches", model: 'Fiche'}, function (err, fiche) {

                        if (err) {
                            res.status(400).send("Populating failed",err);
                        }

                        applyModelVersion(Fiche, req);

                        return res.json(fiche);
                    });
                });
            });
        });
    });
}

export function removeRelation(req, res) {

    Fiche.findOne({ _id : req.params.id}, function (err, fiche) {
        if (err) {
            res.status(500).send("fiche not found",err);
        }

        Fiche.findOne({ _id: req.body.fiche._id}, function (err, ficheToAdd) {

            fiche.related_fiches = fiche.related_fiches.filter(function(el) {
                return el.toString() !== ficheToAdd._id.toString();
            });

            ficheToAdd.related_fiches = ficheToAdd.related_fiches.filter(function(el) {
                return el.toString() !== fiche._id.toString();
            });


            fiche.save((err, fiche) => {
                ficheToAdd.save((err, ficheToAdd) => {

                    Fiche.populate(fiche, {path: "related_fiches", model: 'Fiche'}, function (err, fiche) {

                        if (err) {
                            res.status(400).send("Populating failed",err);
                        }

                        applyModelVersion(Fiche, req);
                        return res.json(fiche);
                    });
                });
            });
        });
    });
}

//will only update the cover.path for now, maybe later we will need other updated concerning the cover
function updateCover(req, res){
  let postParams = req.body,
      ficheId = req.params.id,
      searchQuery = {'_id': ficheId},
      //carefull here, since we don't update the file, some of the cover fields are will be not udpated like size, destination and so on
      updateQuery = {'cover.path': postParams.remotePath};

  Fiche.update(searchQuery, updateQuery, function(err, result){
    if (err){
      res.status(500).send("fiche not found", err);
    }

    applyModelVersion(Fiche, req);
    return res.json(result);
  });
}

function updateTagsIfNeeded(oldFiche, req, res) {
  const oldTags = oldFiche.tags,
      newTags = req.body.tags;

  if (newTags){
    let diff = oldTags.length > newTags.length ?
        oldTags.diff(newTags):
        newTags.diff(oldTags);
    //seems we use this route so we need to compare the receiving tags from here
    if (diff.length){
      //need to update those fucking tags mouahaha
      TagController.countCallback(diff, 0, []);
    }
  }
}

function createAssociatedPlaylistsShellFunction(oid){
    return db.playlists.find({linkedFiches: {$in: db.linkedfiches.distinct("_id", {fiche: oid})}}).map(function(playlist){
        playlist.realFiches = playlist.linkedFiches.map(function(ficheId){

            //need to find the current fiche biatch!!!
            var link =  db.linkedfiches.findOne({"_id": ficheId});

            return db.fiches.findOne({"_id": link.fiche});
        })
        return playlist;
    });
}

//overrode req and res params, to reuse this function when we have a list of fiches that need their associated playlists,
//removed res.status and res.json in order to rewrite headers since we don't need to on user controller
function getAssociatedPlaylists(req, res, extra = null) {
  const ficheId = req.params.id;

  //let start = new Date(), end;

  //console.log('before eval, ficheId: ' + ficheId);

  //LinkedFiche.find({fiche: ficheId}, (err, linkedFiches) => {
  LinkedFiche.distinct("_id", {fiche: ficheId}, (err, linkedFiches) => {
    if (err){

      errMessage = "Error in calling stored function getAssociatedPlaylists ficheId> " + ficheId + "#mess> " +  err;

      if (res.status){
        return res.status(500).send(errMessage);
      }else {
        return errMessage = "Error in calling stored function getAssociatedPlaylists ficheId> " + ficheId + "#mess> " +  err;
      }
    }

    Playlist.find({linkedFiches: {$in: linkedFiches}, status: {$in: status.PUBLISHED}}, (err, result) => {

      let errMessage;

      if (err){

        errMessage = "Error in calling stored function getAssociatedPlaylists ficheId> " + ficheId + "#mess> " +  err;

        if (res.status){
          return res.status(500).send(errMessage);
        }else {
          return errMessage = "Error in calling stored function getAssociatedPlaylists ficheId> " + ficheId + "#mess> " +  err;
        }

      }

      let playlists = result;

      LinkedFiche.populate(result, [{
          path: 'linkedFiches',
          model: 'LinkedFiche'
      }], (err, innerResults) => {

        if (err) {
          return res.status(500).send("Something wrong in eval and multiple population #mess > " + err);
        }


        Fiche.populate(innerResults, [{
          path: 'linkedFiches.fiche',
          model: 'Fiche'
        }, {
          path: 'labels.categories',
          model: 'Category'
        }, {
          path: 'labels.tags',
          model: 'Tag'
        }], (err, outerResults) => {
          let toReturn = outerResults;

          if (res.json){
            applyModelVersion(Playlist, req, {forList: true});
            applyModelVersion(LinkedFiche, req, {forList: true});
            applyModelVersion(Fiche, req, {forList: true});
            applyModelVersion(User, req);
            return res.json({playlists: toReturn});
          }

          //end = (new Date().getTime() - start.getTime()) / 1000;

          //console.log(end);

          if (extra) {
            extra.arr.playlists = [...extra.arr.playlists, ...toReturn];

            let toAppend = toReturn.map((play) => {
              return {
                ficheId: ficheId,
                playId: play._id
              }
            });

            extra.arr.matchingFicheIds = [...extra.arr.matchingFicheIds, ...toAppend];
            //return extra.cb();
            return extra.cb(null, outerResults);
          }

          applyModelVersion(Playlist, req, {forList: true});
          applyModelVersion(LinkedFiche, req, {forList: true});
          applyModelVersion(Fiche, req, {forList: true});
          applyModelVersion(User, req);

          return toReturn;
        });
      });
    });
  });
}

//shall return the heart strokes picked by Traverse team,
//for now it will only return some random fiches
function getHeartStroke(req, res) {

  const limit = req.params.limit || 5,
        searchQuery = [
          { $match: { status: 1 } },
          { $sample: { size: limit } }
        ];

  Fiche.aggregate(searchQuery, (err, results) => {
    if (err) {
      return res.status(500).send("Something wrong in retrieving randomized #mess > " + err);
    }

    Fiche.populate(results, [{
        path: 'categories',
        model: 'Category'
    },{
        path: 'tags',
        model: 'Tag'
    },{
        path: 'created_by',
        model: 'User'
    }], (err, innerResults) => {

      if (err) {
        return res.status(500).send("Something wrong in aggregation and multiple population #mess > " + err);
      }

      let toReturn = forceDefaultValues(innerResults);

      applyModelVersion(Fiche, req);
      return res.json({fiches: toReturn});
    });
  });
}

export function getTypes(req, res) {
  Fiche.distinct('__t', (err, ficheTypes) => {
    if (err) {
      return res.status(500).send("Something wrong in retrieving types > " + err);
    }
    return res.json({
      types: ficheTypes
    });
  });
}

export function searchForCreator(req, res, toSend, userId) {

  User.populate(toSend, [{
    path: 'created_by',
    model: 'User'
  }], (err, result) => {
    if (err) {
      return res.status(500).send("Something wrong in retrieving types > " + err);
    }

    return res.json(result);
  });
}

export function stat(req, res) {
  return mixinStat(Fiche, req, res);
}

export function like(req, res) {
  return mixinLike(Fiche, req, res, (err, fiche) => {
    return sendBackFullFiche(req, res, fiche, false);
  });
}

export function dislike(req, res) {
  return mixinDislike(Fiche, req, res, (err, fiche) => {
    return sendBackFullFiche(req, res, fiche, false);
  });
}

function sendBackFullFiche(req, res, toSend, forList = false, hasToStringify = false, callback = null) {

  applyModelVersion(Fiche, req, {forList: forList});
  applyModelVersion(User, req, {forList: forList});

  if (callback != null) {
    return callback(hasToStringify? JSON.stringify(toSend): toSend);
  }else {
    return res.json(toSend);
  }
}

/**
 * Will update only the fiches here
 * the structure of the newThemes must be an array of
 * {
 *    oldName: String,
 *    newName: String,
 *    //below if not present we don't care since there is a test inside this recursive call
 *    newColor: HexColor
 * }
 *
 * TODO, do we need oldThemes here ?
 */
export function updateInnerThemeByName(newThemes, oldThemes, callback) {

  const findThemeIndex = (themes, toSeek) => {
    return themes.map((theme) => {
      return theme.name;
    }).indexOf(toSeek);
  };

  //splice (-1,1), return the last item (inside an array) and remove the last item from the list
  if (newThemes.length > 0) {
    let lastTheme = newThemes.splice(-1, 1)[0],
        { oldName } = lastTheme,
        { newName } = lastTheme;

    Fiche.find({"themes.name": oldName}, (err, fiches) => {
      if (err) {
        return callback(err, null);
      }

      console.log("hum...", oldName);

      fiches = fiches.map((currentFiche) => {
        //seek for the theme which has the same name
        let themeIndex = findThemeIndex(currentFiche.themes, oldName);

        //console.log("bouark", previousFicheThemes, oldName, themeIndex);

        if (themeIndex !== -1) {
          currentFiche.themes[themeIndex].name = newName;

          if (lastTheme.newColor) {
            currentFiche.themes[themeIndex].color = lastTheme.newColor;
          }

          console.log("hey baby", currentFiche.themes, themeIndex);
        }

        //at this point all the fiches that contains the theme name would be updated but not saved
        return currentFiche;
      });

      let asyncFicheSave = fiches.map((fiche) => {
        return (done) => {

          //since it has the same length of a nested object, it's a bit akward but works
          fiche.markModified('themes')

          fiche.save((err) => {
            if (err) {
              return callback(err, null);
            }
            done();
          });
        };
      });

      async.series(asyncFicheSave, function allTaskCompleted() {
        console.log('@FicheController > updateFicheThemes, all done');

        //one theme done, moving onto next one
        return updateInnerThemeByName(newThemes, oldThemes, callback);
      });
    });
  }else {
    //all update done from here, so we need to send back a result
    return callback(null, 200);
  }
}

export default{
    all,
    one,
    add,
    addRelation,
    removeRelation,
    remove,
    update,
    search,
    updateTagsIfNeeded,
    updateCover,
    getAssociatedPlaylists,
    getHeartStroke,
    updateGeoPositionIndexIfNeeded,
    getTypes,
    searchForCreator,
    stat,
    like,
    dislike,
    updateInnerThemeByName
}
