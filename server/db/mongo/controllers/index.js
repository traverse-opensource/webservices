import { applyModelVersion } from '../static';

import users from './users'
import fiches from './fiches'
import playlists from './playlists'
import categories from './categories'
import events from './events'
import media from './media'
import objects from './objects'
import places from './places'
import people from './people'
import tags from './tags'
import themes from './themes.js'
import heartstrokes from './heartstrokes.js'
import groups from './groups.js'
import entity from './entity'
import filters from './filters';
import slugs from './slugs';
import feeds from './feeds';

//TODO check why we need this twice ?
//export { users, fiches, playlists, categories, events, media, objects, places, people, tags, themes, heartstrokes, groups, entity, filters, slugs, feeds };

export default {
  users,
  fiches,
  playlists,
  categories,
  events,
  media,
  objects,
  places,
  people,
  tags,
  themes,
  heartstrokes,
  groups,
  entity,
  filters,
  slugs,
  feeds
};
