import Group from '../models/groups';
import { permission } from '../constant';
import { applyModelVersion } from '../static';

/**
 * List
 */
export function all(req, res) {
  let showAll = false;

  if (req.query && req.query.all) {
    showAll = req.query. all;
  }
  let query = showAll ?
      {}:
      { title: {$ne: permission.DEACTIVATED} }
  Group.find(query).exec((err, groups) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    applyModelVersion(Group, req);
    return res.json(groups);
  });
}

/**
 * Add a Group
 */
export function add(req, res) {
  Group.create(req.body, (err,group) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    applyModelVersion(Group, req);
    return res.status(200).send(group);
  });
}

export default {
  all,
  add
};
