import _ from 'lodash';
import Entity from '../models/entity';
import { applyModelVersion } from '../static';

/**
 * List
 */

export function all(req, res) {
  
  Entity.find({}).exec((err, entities) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    applyModelVersion(Entity, req);
    return res.json(entities);
  });
}

/**
 * Add a Entity
 */
export function add(req, res) {
  Entity.create(req.body, (err,entity) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }
    applyModelVersion(Entity, req);
    return res.status(200).send(entity);
  });
}

/**
 * Update a Entity
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);

  Entity.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a Entity
 */
export function remove(req, res) {
  const query = { _id: req.params.id };
  console.log(query);
  Entity.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }
    console.log(query);
    return res.status(200).send('Removed Successfully');
  });
}
export function one(req,res){
  const query = {_id: req.params.id};
  Entity.findOne(query).exec((err, entity) => {
    if(err){
      res.status(500).send('Could not retrieve the entity with the id '+req.params.id)
    }
    applyModelVersion(Entity, req);
    return res.status(200).send(entity)
  })
}
export default {
  all,
  add,
  update,
  remove,
  one
};
