import _ from 'lodash';
import mongoose from 'mongoose';
//models
import Fiche from '../models/fiches';
import Playlist from '../models/playlists';
import User from '../models/users';
//controllers
import UserController from './users';
import { HASH_TYPES, status, PAGINATION_DEFAULT, LOCATION_DEFAULT } from '../constant.js';
import { createSkipQuery, sortByRankingAlgorithm, applyModelVersion } from '../static';

import format from 'string-template';
import LRU from 'lru-cache';

const options = {
  max: 500,
  length: function (n, key) { return n * 2 + key.length },
  //needs to refresh every 8 hours
  maxAge: 15 * 60 * 1000//8 * 60 * 60 * 1000
};
const cache = LRU(options);

const EXPLORE_MODE_BASE_LIMIT = 30000;

//TODO maybe we need to adjust to the max size the cache
//otherCache = LRU(50); // sets just the max size

/**
 * Convenient class to do some search or other filtering
 * Will use other search method and will create some if the algorithm is not fast enough
 *
 * What wee expect from this class for now is to return fiches nor playlists item
 * On mobile we call them Mixin Object, can be either a fiche nor a playlist
 */

function handleError(req, res, consoleMessage, reponseMessage, err = null, code = 500) {
  if (err) {
    console.log("filters.js @handleError > " + consoleMessage + "> " + err);
    return res.status(code).send(reponseMessage + err);
  }
  return false;
}

function transformIntoArray(toTransform = null) {
  if (toTransform !== null) {
    if (!Array.isArray(toTransform)){
      return [toTransform];
    }
    //no need transformation => just send back the object that must be an array at this step
    return toTransform;
  }
  return toTransform;
}

//must use an array here, if only got one element just transform it into an array of one element
function checkIds(arrayToCheck) {
  arrayToCheck.forEach((item) => {
    if (!mongoose.Types.ObjectId.isValid(item)){
      return false;
    }
  });
  return true;
}

function createTagsQuery(req, res, tags = null) {

  let toReturn = {};

  if (tags !== null && checkIds(tags)) {
    toReturn = {
      tags: {
        $in: tags
      }
    };
  }
  return toReturn;
}

function createThemesQuery(req, res, themes = null) {
  return themes !== null ? {
    "themes.name": {
      $in : themes
    }
  }: {};
}

function createCategoriesQuery(req, res, categories = null) {

  let toReturn = {};

  if (categories !== null && checkIds(categories)) {
    toReturn = {
      categories: {
        $in: categories
      }
    }
  }

  return toReturn;
}


function checkTypes(toCheck) {
  toCheck.forEach((type) => {
    if (HASH_TYPES.indexOf(type) === -1) {
      return false;
    }
  })
  return true;
}

function createTypesQuery(req, res, ficheTypes = ["Places","People","Media","Objects","Events"]) {

  let toReturn = {};

  if (ficheTypes !== null && checkTypes(ficheTypes)) {
    toReturn = {
      __t: {
        $in : ficheTypes
      }
    }
  }

  return toReturn;
}

//only published ones since it's for mobile query, so the user don't need to see unpublished fiches nor draft ones
function createStatusFilter() {
  return {
    status: status.PUBLISHED
  };
}

function createGeolocLocation(max, lat, lng) {
  let nearQuery = {type: "Point", coordinates: [lng, lat]};

  let toReturn = {
    location: {
      '$near': {
        '$maxDistance': max,
        '$geometry': nearQuery
      }
    }
  };

  return toReturn;
}

//generic method to send back the result wether it comes from the cache nor from the database
function returnResults(params) {
  const { req, res, page, limit, mixinList, cacheName, userCacheName } = params;

  let first = createSkipQuery(page, limit);
  let toReturn = mixinList.slice(first, first + limit);

  //if results comes from DB then we need to cache it
  if (userCacheName) {
    //let currentCache = cache.get(userCacheName);

    let newCache = [];
    newCache[cacheName] = mixinList;

    cache.set(userCacheName, newCache);
  }

  //applies model version for any requests
  applyModelVersion(Fiche, req, {forList: true});
  applyModelVersion(Playlist, req, {forList: true, populateLinkedFiches: false});
  applyModelVersion(User, req, {fromAll: true});

  return res.json(toReturn);
}

function searchAndComputeFromCache(params) {

  const { req, res, page, limit, cacheResults } = params;

  let mixinList = cacheResults;
  return returnResults({ req, res, page, limit, mixinList });
}

function searchAndComputeFromDB(params) {

  const { req, res, finalQuery, lat, lng, includesPlaylist, page, limit, cacheName, isExploreMode, userCacheName } = params;

  let toExec = Fiche.find(finalQuery);

  return toExec.exec((err, foundFiches) => {
    if (!handleError(req, res, 'Error in combining queries', 'Whoops something went wrong', err)) {

      //in all case we need to rank,
      //then we check if we use playlists
      //at then end we paginate
      res.setHeader('Content-Type', 'application/json');

      //mid = new Date();

      return sortByRankingAlgorithm(
        foundFiches,
        {
          latitude: lat,
          longitude: lng,
          isExploreMode
        },
        (sortedFiches) => {
          if (parseInt(includesPlaylist)) {
            return UserController.retrievePlaylistHolders(sortedFiches, req, res, (mixinList) => {

              return returnResults({ req, res, page, limit, mixinList, cacheName, userCacheName });
            });
          }else {
            let mixinList = sortedFiches;
            return returnResults({ req, res, page, limit, mixinList, cacheName, userCacheName });
          }
        });
    }
  });
}

//let start, mid, end;

/*const debugTime = () => {
  end = new Date();

  end = (end.getTime() - start.getTime()) / 1000;
  mid = (mid.getTime() - start.getTime()) / 1000;

  console.log("includePlaylist > mid > ", mid, " > end > ", end);
};*/

function createCachePrefix(prefix, objArr) {
  return prefix + objArr.map((obj) => {
    return obj.substring(0, 3);
  }) + "_";
}

function createUserCacheName(userAgent, clientIP, uniqueId) {

  return format('{userAgent}_{clientIP}_{uniqueId}', {
    userAgent: userAgent,
    clientIP: clientIP,
    uniqueId: uniqueId
  });

}

function createCacheName(apiVersion, tags, themes, categories, types, includesPlaylist, maxDistance, lat, lng) {

  let toReturn = null;

  if (!tags) {
    let trailing = "";

    if (themes) {
      trailing += createCachePrefix("th", themes);
    }
    if (categories) {
      trailing += createCachePrefix("ca", categories);
    }
    if (types) {
      trailing += createCachePrefix("ty", types);
    }

    //round position with 3 digit after dot
    lat = Number(lat.toFixed(3));
    lng = Number(lng.toFixed(3));

    toReturn = format('{apiVersion}_{lat}_{lng}_{radius}_{includesPlaylist}_{trailing}', {
      apiVersion: apiVersion,
      lat: lat,
      lng: lng,
      radius: maxDistance,
      includesPlaylist: includesPlaylist,
      trailing: trailing.length > 0 ? trailing.slice(0, -1): trailing
    });
  }

  //don't need to store a cache for tags, it's too much
  return toReturn;
}

/**
 * entry method that will interpret the search query and will only fire once the whole queries have been computed
 * seems that we can have fiches results, fiches mixed with playlists and also only playlists
 */
export function search(req, res) {

  //start = new Date();

  //checking the parameters
  const body = req.query;
  let { tags, themes, categories, types } = body,
      includesPlaylist = body.includesPlaylist? body.includesPlaylist : 1,
      //Geolocation based
      maxDistance = body.maxDistance ? parseInt(body.maxDistance): LOCATION_DEFAULT.MAX_DISTANCE,
      lat = body.lat? parseFloat(body.lat): LOCATION_DEFAULT.LAT,
      lng = body.lng? parseFloat(body.lng): LOCATION_DEFAULT.LNG,
      //pagination system
      page = body.page? parseInt(body.page): PAGINATION_DEFAULT.PAGE,
      limit = body.limit? parseInt(body.limit): PAGINATION_DEFAULT.LIMIT,
      andQuery = {},
      finalQuery = {$and: {}},
      //mandatory since we need to send back result based on json transformation that depends on the versionId
      apiVersion = req.params.versionId,

      userAgent = req.headers['user-agent'],
      clientIP = req.headers['x-forwarded-for'],
      clientUniqueId = req.headers['x-unique-id'];

      console.log("test", "gone here", req.originalUrl, "**************************************");
      console.log("debug", "userAgent", userAgent, "**************************************");
      console.log("debug", "clientIP", clientIP, "**************************************");
      console.log("debug", "clientUniqueId", clientUniqueId, "**************************************");

  /*console.log(body);
  console.log(req.query);
  console.log(req.params);*/

  tags = transformIntoArray(tags);
  themes = transformIntoArray(themes);
  categories = transformIntoArray(categories);
  types = transformIntoArray(types);

  //building mongoose search query
  andQuery = [
    createTagsQuery(req, res, tags),
    createThemesQuery(req, res, themes),
    createCategoriesQuery(req, res, categories),
    createTypesQuery(req, res, types),
    createStatusFilter(),
    createGeolocLocation(maxDistance, lat, lng)
  ].filter((item) => {
    //just removing useless filters
    return Object.keys(item).length !== 0;
  });

  finalQuery.$and = andQuery;

  //if contains tags just return null
  let userCacheName = createUserCacheName(userAgent, clientIP, clientUniqueId);
  let cacheName = createCacheName(apiVersion, tags, themes, categories, types, includesPlaylist, maxDistance, lat, lng);

  let isExploreMode = maxDistance > EXPLORE_MODE_BASE_LIMIT;
  let userCacheResults = cache.get(userCacheName);


  if (userCacheResults) {
    let cacheResults = userCacheResults[cacheName];

    if (cacheResults) {
      return searchAndComputeFromCache({ req, res, page, limit, cacheResults });
    }
  }
  
  return searchAndComputeFromDB({ req, res, finalQuery, lat, lng, includesPlaylist, page, limit, cacheName, isExploreMode, userCacheName });

}

export default {
  search
};
