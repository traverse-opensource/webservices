import HeartStroke from '../models/heartstrokes'
import User from '../models/users'
import Fiche from '../models/fiches'
import Playlist from '../models/playlists'
import { CONSTANTS, applyModelVersion } from '../static';

const DB_REFRESH = 250;

function handleError(req, res, customMessage, err, clientMessage) {
  console.log(customMessage, err);
  return res.status(500).send({message: clientMessage});
}

export function all(req, res) {
    HeartStroke.find({ status: CONSTANTS.STATUS.PUBLISHED }).exec((err, results) => {
      if (err) {
        return handleError(req, res, 'Error in getting the heartstrokes -> function /all', err, 'Something went wrong getting the themes list');
      }

      //since the ficheid and the playlistid are auto populated, we just need to unwind the result
      let toReturn = results
        .sort((a, b) => {
          return a.order > b.order;
        })
        .map((item) => {
          return item.collection_type === CONSTANTS.TYPES.FICHE_TYPE ?
            item.fiche_id: item.playlist_id;
        });

      applyModelVersion(User, req);
      applyModelVersion(Fiche, req);
      applyModelVersion(Playlist, req);

      return res.json(toReturn);  
    })
}

export default{
    all
}
