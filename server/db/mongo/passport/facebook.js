import User from '../models/users';

/* eslint-disable no-param-reassign */
export default (req, accessToken, refreshToken, profile, done) => {
  
  //since access token is undefined, just reuse it to use babel for the user.token.push on facebook strategy
  accessToken = refreshToken.access_token;
  
  if (req.user) {
    
    return User.findOne({ "social.value": profile.id }, (findOneErr, existingUser) => {
      if (existingUser) {
        return done(null, false, { message: 'There is already a Facebook account that belongs to you. Sign in with that account or delete it, then link it with your current account.' });
      }
      return User.findById(req.user.id, (findByIdErr, user) => {
        user.social = {kind: 'facebook', value: profile.id};
        user.tokens.push({ kind: 'facebook', accessToken });
        user.profile.name = user.profile.name || profile.displayName;
        user.profile.gender = user.profile.gender || profile.gender;
        
        //find the good stuff here
        user.profile.picture = user.profile.picture || profile._json.picture.url;
        
        user.save((err) => {
          done(err, user, { message: 'Facebook account has been linked.' });
        });
      });
    });
  }
  
  return User.findOne({ "social.value": profile.id }, (findByFacebookIdErr, existingUser) => {
    if (existingUser) return done(null, existingUser);
    
    return User.findOne({ email: profile._json.email }, (findByEmailErr, existingEmailUser) => {
      if (existingEmailUser) {
        return done(null, false, { message: 'There is already an account using this email address. Sign in to that account and link it with Facebook manually from Account Settings.' });
      }
      const user = new User();
      user.email = profile._json.email;
      user.social = {kind: 'facebook', value: profile.id};
      user.tokens.push({ kind: 'facebook', accessToken });
      user.profile.name = profile.displayName;
      user.profile.gender = profile.gender;
      user.profile.picture = profile._json.picture.url;
      
      return user.save((err) => {

        done(err, user, { message: 'User has been created with Facebook account.' });
      });
    });
  });
};
/* eslint-enable no-param-reassign */
