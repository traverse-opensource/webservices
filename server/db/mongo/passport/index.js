import deserializeUser from './deserializeUser';
import google from './google';
import facebook from './facebook';
import twitter from './twitter';
import instagram from './instagram';
import local from './local';

export { deserializeUser, google, local };

export default {
  deserializeUser,
  google,
  facebook,
  twitter,
  instagram,
  local,
};
