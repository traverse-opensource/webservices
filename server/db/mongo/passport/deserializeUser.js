import User from '../models/users';

export default (id, done) => {
  User.findById(id, (err, user) => {
    
    if (err) {
      //instagram case, just return insta user which is mobilethinking
      return done(null, id);
    }
    
    return done(err, user);
  });
};
