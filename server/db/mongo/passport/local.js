import User from '../models/users';

function compareBodyItems(email, password) {
  if (email !== null, password !== null) {
    return true;
  }
  return false;
}

export default (email, password, done) => {
  
  if (!compareBodyItems(email, password)) {
    return done(null, false, { message: 'Your email or password combination is not correct.' });
  }
  
  return User.findOne({ email }, (findErr, user) => {
    if (!user) return done(null, false, { message: `There is no record of the email ${email}.` });
    return user.comparePassword(password, (passErr, isMatch) => {
      if (isMatch) {
        return done(null, user);
      }
      return done(null, false, { message: 'Your email or password combination is not correct.' });
    });
  });
};
