/**
 * This file holds generic methods to compute some fields for all controllers and models
 */
import geolib from 'geolib';
import writeFileAtomic from 'write-file-atomic';
import json2csv from 'json2csv';
import { SERVER_HOSTS } from './constant';

//heartstrokes
const CONSTANTS = {
  TYPES: { FICHE_TYPE: 0, PLAYLIST_TYPE: 1 },
  STATUS: { DRAFT: 0, PUBLISHED: 1, DELETED: 2},
  SOCIAL: {
    TYPES: { INSTAGRAM: 0, FACEBOOK: 1, TWITTER: 2},
    PAGINATION: { LIMIT: 30 },
    REFRESH_SERIES: 75,
    REFRESH_SERIES_SYNC: 50,
    REFRESH_SERIES_ITEM_SAVE: 10
  },
  MANUAL: {
    EVENT_SKIP: 3
  }
};

// don't need to export this one
const createDefaultTheme = () => {
  return {
    name: "Default",
    color: "#386F8D",
    description: "",
    ponderation: 0
  }
}

const sortThemesByPonderation = (themes) => {
  return themes.sort((a, b) => {
    if (a.ponderation < b.ponderation) {
      return 1;
    }else if (a.ponderation > b.ponderation) {
      return -1;
    } else {
      return 0;
    }
  });
}

/*
 * return the theme dominant for a fiche, should not be used here
 * todo make it working for single fiche also
 */
const FicheThemeDominant = (fiche) => {
  let { themes } = fiche;
  let toReturn = createDefaultTheme();

  let sortedThemes = sortThemesByPonderation(themes);

  if (Object.keys(sortedThemes).length > 0) {
    let firstKey = Object.keys(sortedThemes)[0];

    toReturn = sortedThemes[firstKey];
  }

  return toReturn;
}

//search inside linkedFiches and theme, compte the max ponderation for each found themes and sort them by the computed ponderation
//just send back the theme that has the most ponderation
//if some themes are equal, then just send the first one on the sorted list
const PlaylistThemeDominant = (playlist) => {
  let {linkedFiches} = playlist;
  let toReturn = createDefaultTheme();

  let themesHolder = [];
  let innerThemes;

  let allThemes = linkedFiches.forEach((linkedFiche) => {

    innerThemes = linkedFiche.fiche.themes;

    innerThemes.forEach((theme) => {
      if (!themesHolder[theme.name]) {
        themesHolder[theme.name] = {
          name: theme.name,
          color: theme.color,
          ponderation: 0,
          description: theme.description
        };
      }

      //update ponderation
      themesHolder[theme.name].ponderation += theme.ponderation
    });
  });

  //we have a hashtable, so it won't work as expected => need to transform into array
  let themes = [];

  for (var key in themesHolder){
    themes.push(themesHolder[key]);
  }

  //once we get all the computed ponderation, just sort the result having totalPonderation the highest one and pick the first one
  let sortedThemes = sortThemesByPonderation(themes);

  //console.log('--sortedThemes', sortedThemes);

  if (Object.keys(sortedThemes).length > 0) {
    let firstKey = Object.keys(sortedThemes)[0];

    toReturn = sortedThemes[firstKey];
  }

  return toReturn;
}

const RANKING_FACTORS = {
  PROXIMITY: 0.2,
  CONNECTIONS: 0.35,
  TYPE: 0.45,
  TYPE_PONDERATION: {
    "Places": 1/5,
    "People": 2/5,
    "Objects": 3/5,
    "Events": 4/5,
    "Media": 5/5
  }
};

//Need this to make javascript not using weird roundings
const strip = (number) => {
    return (parseFloat(number).toPrecision(12));
}

/**
 * Return the score from 1 fiche based distance from location, its fiches connections and also its fiche type
 */
const computeScore = (fiche, currentDistance, distanceMax, isExploreMode = false) => {
  const currentType = fiche.__t;

  const connectionNumber = (fiche.related_fiches?
        fiche.related_fiches.length: 0) + 1;

  let scoreDistance = strip(RANKING_FACTORS.PROXIMITY * (currentDistance / distanceMax));
  //TODO ensures related_fiches exists!
  let scoreConnections = !isExploreMode? strip(RANKING_FACTORS.CONNECTIONS * (1 / connectionNumber)): 0;
  let scoreType = !isExploreMode? strip(RANKING_FACTORS.TYPE * RANKING_FACTORS.TYPE_PONDERATION[currentType]): 0;

  let toReturn = parseFloat(scoreDistance) + parseFloat(scoreConnections) + parseFloat(scoreType);

  return toReturn;
}

/*
 * the mixinItems argument holds fiches and playlists
 * callback should never be null since we need to apply the pagination from here,
 * be carefull, it is the the mongo native pagination,
 * we need to use the same logic at the createSkipQuery stated below
 *
 * @params fiches, are the list of fiches that comes from the inner query
 * @params seekParams is the location that we need to sort by distance, the $near query on mongo already sort by distance, but we don't have the distance,
 *  so we need to compute it from here
 * @params callback, is the function callback, we need to ask res to send a puurfect json ;)
 */
const sortByRankingAlgorithm = (fiches, seekParams, callback) => {

  const locationToMeasure = {
    latitude: seekParams.latitude,
    longitude: seekParams.longitude
  };

  let distances = fiches.map((fiche) => {
    let coordinates = fiche.location.coordinates,
        location = {
          latitude: coordinates[1],
          longitude: coordinates[0]
        }
    return geolib.getDistance(location, locationToMeasure);
  });

  //returns a copy of distances so that distances won't be affected by the results of the sorting
  let sortedDistances = distances.slice(0);

  //sort from greater to lower in order to have the highest distance on first element
  sortedDistances.sort((a, b) => {
    if (a < b) {
      return 1;
    }else if (a > b) {
      return -1;
    } else {
      return 0;
    }
  });

  let distanceMax = sortedDistances[0];

  /*console.log("---------------", distances);
  console.log("---------------", sortedDistance);
  console.log("---------------", distanceMax);*/

  let toReturn = fiches
    //first map, creates a custom item with score and fiche to retrieve
    .map((fiche, index) => {

      let distance = distances[index];

      return {
        id: fiche._id,
        score: computeScore(fiche, distance, distanceMax, seekParams.isExploreMode),
        fiche: fiche
      }
    //secondly sort items by lowest score
    })
    .sort((a, b) => {
      if (a.score < b.score) {
        return -1;
      }else if (a.score > b.score) {
        return 1;
      } else {
        return 0;
      }
    })
    //thirdly retrieve the ficheItem
    .map((customItem) => {
      return customItem.fiche
    });

    if (seekParams.isExploreMode) {
      return callback(toReturn);
    }else {
      //extract events that are not passed yet
      let notPassed = [];
      let subSorted = [];

      let today = new Date().getTime();
      let ficheEndDate;

      toReturn.forEach((fiche) => {
        if (fiche.__t === "Events"){
          ficheEndDate = new Date(fiche.end_date).getTime();
          if (ficheEndDate >= today) {
            notPassed.push(fiche);
          }else {
            subSorted.push(fiche);
          }
        }else {
          subSorted.push(fiche);
        }
      });

      toReturn = [];
      let notPassedIndex = 0,
          subSortedIndex = 0;
      //insert every MANUAL.EVENT_SKIP
      for (let i = 0, len = notPassed.length; i < len; ++i) {
        subSorted.splice(i * CONSTANTS.MANUAL.EVENT_SKIP, 0, notPassed[i]);
      }

      return callback(subSorted);
    }
}

/**
 * return a mongoose structure for the pagination system
 */
const createSkipQuery = (pageNumber, nPerPage) => {
  return pageNumber > 0 ? ((pageNumber-1)*nPerPage) : 0;
}

const FICHE_FILTERS = { ALL: 0, ALL_DRAFT: 1, MY_PUBLISHED: 2, MY_DRAFT: 3 };

const status = { DRAFT: 0, PUBLISHED: 1, DELETED: 2 };

const FICHE_TYPES = {
  PLACES: "Places",
  PEOPLE: "People",
  MEDIA: "Media",
  OBJECTS: "Objects",
  EVENTS: "Events"
};

/**
 * Will be usefull in all models in order to send the json based on send model
 * if no version are found, we just send the default version
 */
const retrieveApiVersion = (req) => {
  /*if (req.params) {
    //return req.params.versionId;
    return req.versionId;
  }
  return 0;*/
  return req.versionId;
}

/**
 * applies different options to the json transformer on runtime so that each request results are different
 */
const applyModelVersion = (Model, req, extraParams = {}) => {
  let version = retrieveApiVersion(req);

  console.log("Model>", Model.collection.collectionName, "version", version, extraParams);

  Model.schema.options.toJSON.apiVersion = version;
  Model.schema.options.toJSON.modelName = Model.collection.collectionName;

  let extraKeys = Object.keys(extraParams);
  extraKeys.forEach((key) => {
    Model.schema.options.toJSON[key] = extraParams[key];
  });
}


/**
 * This function ensures that if the hash table of the changes <HASH_CHANGES> has no implementation for a given version, it will just skip it
 * So that the algorithm nor the server will be impacted
 * Also it will ensure that the lower version are performed, so we need to have a migration process between the hash changes going upward
 */
const performChangesBasedOnApiVersion = (doc, ret, options, HASH_CHANGES) => {
  let keys = Object.keys(HASH_CHANGES)
  //ensures the HASH_CHANGES is sorted
  .sort((v1, v2) => {
    return parseFloat(v1) >= parseFloat(v2);
  });

  let toReturn = ret;

  let hashLoop = [],
    targetIndex = targetIndex = keys.indexOf(options.apiVersion) > -1 ? keys.indexOf(options.apiVersion): 0;



  for (let i = 0; i <= targetIndex; ++i) {
    toReturn = HASH_CHANGES[keys[i]](doc, toReturn, options);
  }

//console.error("--------", arguments.length, "----------");
//console.log("real yolo ++++", keys, targetIndex, "+++++", options.modelName, options.apiVersion, "+++++");

  return toReturn;
}

const mixinStat = (Model, req, res) => {
  Model.aggregate([
    {"$group" : {_id:"$status", count:{$sum:1}}}
  ], (err, results) => {
    if (err) {
      return res.status(500).send("Something wrong in retrieving stats > " + err);
    }

    return res.json(results);
  })
}


/**
 * Generic method to seek and update an array based on its Model and its id
 * The array is altered through the callback
 */
const performArrayUpdateBasedOnModel = (Model, req, res, callback) => {
  const query = {_id : req.params.id};
  const { objectId } = req.body;
  const { objectIds } = req.body;

  Model
    .findOne(query)
    .exec((err, item) => {
      if(err) {
        console.log('Error in first query @StaticController.one', err);
        return res.status(500).send('Something went wrong getting the data' + err);
      }

      //console.log("yolo", objectId, objectIds, req.body);

      applyModelVersion(Model, req);
      return callback(item, objectId ? objectId: objectIds);
    });
}

const mixinLike = (Model, req, res, callback = null) => {
  return performArrayUpdateBasedOnModel(Model, req, res, (mixinItem, userId) => {
    //only adds a new like if not already in list
    if (mixinItem.userIds.indexOf(userId) === - 1) {
      mixinItem.userIds.push(userId);
    }

    mixinItem.save((err, newFields) => {
      if (callback != null) {
        return callback(null, mixinItem);
      }
    });
  });
}

const mixinDislike = (Model, req, res, callback = null) => {
  return performArrayUpdateBasedOnModel(Model, req, res, (mixinItem, userId) => {
    //only removes a like if already in list
    let index = mixinItem.userIds.indexOf(userId);
    if (index !== -1) {
        mixinItem.userIds.splice(index, 1);
    }

    mixinItem.save((err, newFields) => {
      if (callback != null) {
        return callback(null, mixinItem);
      }
    });
  });
}

const retrieveHostConfigurations = () => {
  const configs = require('../../../config/config.json');

  const buildBuildHostResponse = (targetHost, protocol, hasToAppendPort) => {
    let toReturn = {
      host: targetHost.addr,
      port: targetHost[protocol],
      protocol: protocol
    };

    if (hasToAppendPort) {
      toReturn.hostAndPort = toReturn.host + ":" + toReturn.port;
    }else {
      toReturn.hostAndPort = toReturn.host;
    }

    return toReturn;
  };

  let ports = configs.port;

  switch(parseInt(process.env.TRAVERSE_CUSTOM_HOST)) {
    case SERVER_HOSTS.LOCAL:
      return buildBuildHostResponse(ports.local, 'http', true);
    case SERVER_HOSTS.MT:
      return buildBuildHostResponse(ports.preprod, 'http', true);
    case SERVER_HOSTS.TRAVERSE:
      return buildBuildHostResponse(ports.api, 'https', false);
    default:
      return null;
  }
}

const recoverServerBasePath = () => {
  let hostConf = retrieveHostConfigurations();

  return hostConf.protocol + "://" + hostConf.hostAndPort;
}

const recoverFeedBasePath = () => {
  return recoverServerBasePath() + "/uploaded_images/feeds";
}

//returns true if the secret key belongs to the editorial platform stuff
const isFromEditorialPlatform = (req) => {
  return req.fromEditorial;
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

const getUniqueStringsInArray = (baseArray) => {
  return baseArray.filter(onlyUnique);
}

const shortenDescription = (baseString, maxChar) => {
  if (!baseString) {
    return '';
  }
  return baseString.length > maxChar?
    baseString.substring(0, maxChar - " [ ... ]".length) + " [ ... ]": baseString;
}

const REQUEST_MODE = {
  HASH_AND_ACCOUNT: 0,
  MULTIPLE_HASHTAGS: 1,
  NO_HASHTAGS: 2,
  SLUG: 3
};

const flatten = list => list.reduce(
  (a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []
);


module.exports = {
  CONSTANTS,
  createDefaultTheme,
  FicheThemeDominant,
  PlaylistThemeDominant,
  sortByRankingAlgorithm,
  createSkipQuery,
  FICHE_FILTERS,
  status,
  FICHE_TYPES,
  retrieveApiVersion,
  applyModelVersion,
  performChangesBasedOnApiVersion,
  mixinStat,
  performArrayUpdateBasedOnModel,
  mixinLike,
  mixinDislike,
  recoverFeedBasePath,
  recoverServerBasePath,
  isFromEditorialPlatform,
  getUniqueStringsInArray,
  shortenDescription,
  REQUEST_MODE,
  flatten
};
