/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import FicheSchema from './fiches'

const MediaSchema = new Schema({
    path: String,
    description: String,
    attachments: {type: String, default: ""},
    presentation: { type: String, default: "" },
    date: {date: String, description: String},
    delta_start: Number
});
export default FicheSchema.discriminator('Media', MediaSchema);