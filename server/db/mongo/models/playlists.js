/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const {Schema} = mongoose;
import autopopulate from 'mongoose-autopopulate';
import { MIXIN_TYPES, status } from '../constant.js';
import { PlaylistThemeDominant, performChangesBasedOnApiVersion } from '../static';

const PlaylistSchema = new Schema({
  name: String,
  description: String,
  created_at: { type: Date, default: Date.now },
  last_updated_at: { type: Date, default: Date.now },
  status: { type: Number, default: status.DRAFT },
  cover: Object,
  created_by: {
    type: Schema.ObjectId,
    ref: 'User',
    autopopulate: true
  },
  last_update_by: {
    type: Schema.ObjectId,
    ref: 'User',
    default: null
  },
  linkedFiches: [{
    type: Schema.ObjectId,
    ref: 'LinkedFiche',
    autopopulate: true
  }],
  labels: {
    categories: [{ type : Schema.ObjectId, ref: 'Category', autopopulate: true }],
    sousCategories: [{type: String}],
    tags: [{ type : Schema.ObjectId, ref: 'Tag', autopopulate: true }]
  },
  userIds: [],
  slug: String
});

PlaylistSchema.plugin(autopopulate);

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    ret.type = MIXIN_TYPES.PLAYLIST;
    ret.theme = PlaylistThemeDominant(ret);
    delete ret.__v;
    //same as fiches
    delete ret.slug;
    return ret;
  },
  "1": (doc, ret, options) => {
    if (!options.populateLinkedFiches) {
      //just returning an array of ficheId if we don't need to populate it
      ret.linkedFiches = ret.linkedFiches.map((linkedFiche) => {
        return linkedFiche.fiche._id;
      });
    }
    
    return ret;
  },
  "1.1": (doc, ret, options) => {
    //add big, medium and thumbnail but thumb by default
    //in this case we have only plublished playlists
    if (ret && ret.cover) {
      let bigPath = ret.cover.path,
          mediumPath = bigPath,
          thumbPath = bigPath;
    
      if (bigPath && bigPath.indexOf("http") === -1) {
        let splitted = bigPath.split("/");
        mediumPath = splitted[0] + "/medium/" + splitted[1];
        thumbPath  = splitted[0] + "/thumb/" + splitted[1];
      }

      let previousCover = ret.cover;

      previousCover.path = mediumPath;
      previousCover.big = bigPath;
      previousCover.thumb = thumbPath;

      ret.cover = previousCover;  
    }
    
    return ret;
  },
  "1.2": (doc, ret, options) => {
    ret.slug = doc.slug;
    return ret;
  }
};

//computing themes color here so that each query on that entity would already have it's theme ponderation
PlaylistSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

export default mongoose.model('Playlist', PlaylistSchema);