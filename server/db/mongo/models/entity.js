import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import { performChangesBasedOnApiVersion } from '../static';

const EntitySchema = new Schema({
    name: String,
    description :String
});

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    delete ret.__v;
    return ret;
  },
  "1": (doc, ret, options) => {
    return ret;
  },
  "1.1": (doc, ret, options) => {
    return ret;
  },
  "1.2": (doc, ret, options) => {
    return ret;
  }
};

EntitySchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

export default mongoose.model('Entity', EntitySchema);
