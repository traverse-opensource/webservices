import GeoJSON from 'mongoose-geojson-schema';
import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
import { MIXIN_TYPES } from '../constant.js';
import { FicheThemeDominant, performChangesBasedOnApiVersion, FICHE_TYPES } from '../static';

const Schema = mongoose.Schema,
    Types = Schema.Types,
    Point = Types.Point,
    ObjectId = Schema.ObjectId;

const status = { DRAFT: 0, PUBLISHED: 1, DELETED: 2 };

const FicheSchema = new Schema({
  name: String,
  created_at: { type: Date, default: Date.now },
  last_updated_at: { type: Date, default: Date.now },
  status: { type: Number, default: status.DRAFT },
  created_by: {type: ObjectId, ref: 'User', autopopulate: true},
  last_update_by: {type: Schema.ObjectId, ref: 'User', default: null},
  related_fiches: [{type: Schema.ObjectId, ref: 'Fiche'}],
  playlists: [{type: Schema.ObjectId, ref: 'Playlist'}],
  cover: Object,
  categories: [{type : Schema.ObjectId, ref: 'Category', autopopulate: true}],
  //TODO check if it's used somewhere
  //types : [{type : Schema.ObjectId, ref: 'TypeSchema'}],
  themes : [{type: Schema.Types.Mixed, default: {}}],
  sousCategories: [String],
  tags : [{type : Schema.ObjectId, ref: 'Tag', autopopulate: true}],
  references : {type: String, default: ""},
  social: {
    type: Schema.Types.Mixed, default: {
      facebook: {tags: [], link: ""},
      instagram: {tags: [], link: ""},
      twitter: {tags: [], link: ""},
      web: {link: ""}
    }
  },
  //putting location system onto fiche itself
  city: String,
  country: String,
  latitude: Number ,
  longitude: Number,
  location: {
    type: Point,
    index: true,
    coordinates: [Number]
  },
  map: Object,
  userIds: [],
  slug: String,
  refreshDates: {
    type: Schema.Types.Mixed
  }
});

FicheSchema.plugin(autopopulate);

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    ret.type = ret.__t;
    ret.theme = FicheThemeDominant(ret);
    delete ret.types;
    delete ret.playlists;
    delete ret.__v;
    delete ret.__t;
    //just debug for now
    delete ret.slug;
    delete ret.refreshDates;

    return ret;
  },
  "1": (doc, ret, options) => {

    if (ret.related_fiches) {
      ret.related_fiches = ret.related_fiches
      .filter((fiche) => {
        return fiche.status === status.PUBLISHED;
      });
    }

    if (options.forList) {
      ret.related_fiches = ret.related_fiches
      .map((fiche) => {
        return fiche._id;
      });
    }
    return ret;
  },
  "1.1": (doc, ret, options) => {

    if (ret && ret.cover) {
      //update cover
      let bigPath = ret.cover.path,
          mediumPath = bigPath,
          thumbPath = bigPath;

      if (bigPath && bigPath.indexOf("http") === -1) {
        let splitted = bigPath.split("/");
        mediumPath = splitted[0] + "/medium/" + splitted[1];
        thumbPath  = splitted[0] + "/thumb/" + splitted[1];
      }

      let previousCover = ret.cover;
      previousCover.path = mediumPath;
      previousCover.big = bigPath;
      previousCover.thumb = thumbPath;

      ret.cover = previousCover;
    }

    if (options.forList) {
      //event case
      switch(ret.type) {
        case FICHE_TYPES.EVENTS:
          ret.description = "";
          break;
        case FICHE_TYPES.PLACES:
          ret.presentation = "";
          ret.history = "";
          break;
      }

      ret.social = {};
      ret.references = "";
      ret.related_fiches = [];
    }

    return ret;
  },
  "1.2" : (doc, ret, options) => {
    //donc forget to standardize data withing this version method
    ret.slug = doc.slug;

    const longDescription = () => {
      let toReturn =
        (doc.presentation != null? doc.presentation: "") +
        (doc.presentation != null? "<br/><br/>": "") +
        (doc.history != null? doc.history: "");
      return toReturn;
    }

    switch(ret.type) {
      case FICHE_TYPES.EVENTS:
        ret.short_description = doc.presentation;
        ret.long_description = doc.description;
        delete ret.presentation;
        delete ret.description;
        break;
      case FICHE_TYPES.MEDIA:
        ret.short_description = doc.description;
        ret.long_description = doc.presentation;
        delete ret.description;
        delete ret.presentation;
        break;
      case FICHE_TYPES.OBJECTS:
        ret.short_description = doc.short_description;
        ret.long_description = longDescription();
        delete ret.presentation;
        delete ret.history;
        break;
      case FICHE_TYPES.PEOPLE:
        ret.short_description = doc.short_bio;
        ret.long_description = doc.presentation;
        delete ret.short_bio;
        delete ret.presentation;
        break;
      case FICHE_TYPES.PLACES:
        ret.short_description = doc.short_description;
        ret.long_description = longDescription();
        delete ret.presentation;
        delete ret.history;
        break;
      default:
        break;
    }

    if (options.forList) {
      ret.long_description = "";
    }

    //delete ret.themes;

    return ret;
  }
};

FicheSchema.options.toJSON = {
    transform: function(doc, ret, options) {
        return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
    }
};

export default mongoose.model('Fiche', FicheSchema);
