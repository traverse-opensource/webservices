/**
 * Defining a User Model in mongoose
 * Code modified from https://github.com/sahat/hackathon-starter
 */
import bcrypt from 'bcrypt-nodejs';
import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
//needed since we dont know the file order durgin webpack compilation
import Group from './groups';
import Entity from './entity'

const {Types: {Mixed}, ObjectId} = mongoose.Schema;

import { performChangesBasedOnApiVersion } from '../static';
/*
 User Schema
 */

const UserSchema = new mongoose.Schema({
  email: { type: String, unique: true, lowercase: true },
  //need to find a way to compare the current password and the attempting creation password
  password: String,
  //remember: group is role and entities are groups
  group: {type: ObjectId, ref: 'Groups', autopopulate: true},
  entities: [{type:ObjectId, ref: 'Entity', autopopulate:true}],
  profile: {
    name: { type: String, default: '' },
    //firstName: { type: String, default: null },
    //lastName: { type: String, default: null },
    gender: { type: String, default: '' },
    //might be manually set by user
    location: { type: String, default: '' },
    website: { type: String, default: '' },
    //should be only a file that was uploaded to our server, then it only matches uploaded_images/profiles nor uploaded_images/users
    //no need to store all meta tags stuff
    picture: { type: String, default: '' }
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  social: {},
  favorites: []
});

UserSchema.plugin(autopopulate);

function encryptPassword(next) {
  const user = this;
  if (!user.isModified('password')) return next();
  return bcrypt.genSalt(5, (saltErr, salt) => {
    if (saltErr) return next(saltErr);
    return bcrypt.hash(user.password, salt, null, (hashErr, hash) => {
      if (hashErr) return next(hashErr);
      user.password = hash;
      return next();
    });
  });
}

/**
 * Password hash middleware.
 */
UserSchema.pre('save', encryptPassword);

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    /*
     * remove social, since not ready on Android yet
     */
    delete ret.social;
    delete ret.password;
    delete ret.tokens;
    delete ret.__v;
    delete ret.lastLoginDate;
    return ret;
  },
  "1": (doc, ret, options) => {
    /*
     * push back social if having
     * group => role
     * entities => groups
     * return only name, email, user id, social tokens if having, profile picture, gender
     * if request from all, don't send email
     */
    ret.social = doc.social;
    ret.role = ret.group;
    ret.groups = ret.entities;
    delete ret.entities;
    delete ret.group;
    if (options.fromAll) {
      delete ret.email;
    }

    return ret;
  },
  "1.1" : (doc, ret, options) => {
    return ret;
  },
  "1.2" : (doc, ret, options) => {
    return ret;
  }
}

UserSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

/*
 Defining our own custom document instance method
 */
UserSchema.methods = {
  comparePassword(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) return cb(err);
      return cb(null, isMatch);
    });
  }
};

/**
 * Statics
 */

UserSchema.statics = {};

export default mongoose.model('User', UserSchema);
