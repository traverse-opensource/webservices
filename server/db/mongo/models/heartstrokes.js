/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
import { CONSTANTS, performChangesBasedOnApiVersion } from '../static';
import autopopulate from 'mongoose-autopopulate';
const Schema = mongoose.Schema;

const HeartStrokeSchema = new Schema({
    collection_type: { type: Number, default: CONSTANTS.TYPES.FICHE_TYPE },
    fiche_id : {
      type: Schema.ObjectId,
      ref: 'Fiche',
      autopopulate: true
    },
    playlist_id: {
      type: Schema.ObjectId,
      ref: 'Playlist',
      autopopulate: true
    },
    status: { type: Number, default: CONSTANTS.STATUS.DRAFT },
    order: Number
});

HeartStrokeSchema.plugin(autopopulate);

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    delete ret.__v;
    return ret;
  },
  "1": (doc, ret, options) => {
    return ret;
  },
  "1.1": (doc, ret, options) => {
    return ret;
  },
  "1.2": (doc, ret, options) => {
    return ret;
  }
};

HeartStrokeSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

export default mongoose.model('HeartStroke', HeartStrokeSchema);
