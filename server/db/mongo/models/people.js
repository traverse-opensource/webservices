/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import FicheSchema from './fiches';

const PeopleSchema = new Schema({
    surname: String,
    birthday : String,
    delta_start: Number,
    deathday: String,
    delta_end: Number,
    place_of_birth: String,
    short_bio: String,
    presentation: String,
    //check with android if we need this also...
    date: {date: String, description: String}
});
export default FicheSchema.discriminator('People', PeopleSchema);