//import GeoJSON from 'mongoose-geojson-schema';
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
      //Types = Schema.Types,
      //Point = Types.Point;
import FicheSchema from './fiches';

const PlaceSchema = new Schema({
    short_description: String,
    presentation: String,
    history: String,
    technical_information: String,
    more_information: String,
    schedule: String,
    accessibility: String,
    contact: String
});
export default FicheSchema.discriminator('Places', PlaceSchema);