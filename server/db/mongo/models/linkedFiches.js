import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';
import { performChangesBasedOnApiVersion } from '../static';
const Schema = mongoose.Schema;


const LinkedFicheSchema = new mongoose.Schema({
    fiche: {
        type: Schema.ObjectId,
        ref: 'Fiche',
        autopopulate: true
    },
    value: String,
    next: {
        type: Schema.ObjectId,
        ref: 'LinkedFiche'
    }
});

LinkedFicheSchema.plugin(autopopulate);

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    delete ret.__v;
    return ret;
  },
  "1": (doc, ret, options) => {
    return ret;
  },
  "1.1": (doc, ret, options) => {
    return ret;
  },
  "1.2": (doc, ret, options) => {
    return ret;
  }
};

LinkedFicheSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

export default mongoose.model('LinkedFiche', LinkedFicheSchema);
