/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import FicheSchema from './fiches'

const ObjectSchema = new Schema({
    short_description: String,
    presentation: String,
    history: String,
    technical_information: String,
    more_information: String,
    date: {date: String, description: String},
    delta_start: Number
});
export default FicheSchema.discriminator('Objects', ObjectSchema);