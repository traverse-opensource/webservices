/**
 * Created by hasj on 21/11/2016.
 */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import { performChangesBasedOnApiVersion } from '../static';

const ThemeSchema = new Schema({
    name: String,
    description: String,
    color: String
});

const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    delete ret.__v;
    return ret;
  },
  "1": (doc, ret, options) => {
    return ret;
  },
  "1.1": (doc, ret, options) => {
    return ret;
  },
  "1.2": (doc, ret, options) => {
    return ret;
  }
};

ThemeSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

export default mongoose.model('Themes', ThemeSchema);
