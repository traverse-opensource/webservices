
import mongoose from 'mongoose';
const
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;
import autopopulate from 'mongoose-autopopulate';
import { performChangesBasedOnApiVersion, recoverFeedBasePath } from '../static';
import { FEED_TYPES, FEED_KINDS, FEED_STATUS, MODEL_LIMITS } from '../constant';

/**
 * this class aims to help people to that are not contributors nor admins to insert media of their journey with traverse at a given fiche location
 */
const FeedSchema = new Schema({
  type: { type: String, default: FEED_TYPES.IMAGE },
  name: { type: String, default: "" },
  //important is the limitation here so that we would send a different response if the description is too big
  description: {
    type: String,
    default: "",
    maxlength: MODEL_LIMITS.FEED_DESCRIPTION_LENGTH
  },
  //shall be only constructed on transformation since we don't need to store this kind of information or maybe not
  //maybe don't need to compute it since we need a viewer and don't want to implement it for now
  permalink: { type: String, default: "" },
  //if saved from our api, must be a traverse feed item
  kind: { type: String, default: FEED_KINDS.TRAVERSE },
  created_at: { type: Date, default: Date.now },
  created_by: {type: ObjectId, ref: 'User', autopopulate: true},
  fiche_id: {type: ObjectId, ref: 'Fiche', autopopulate: true},
  cover: {
    type: Schema.Types.Mixed,
    default: {
      path: "",
      big: "",
      thumb: ""
    }
  },
  //shall not be present every time so no need to give a default value
  video: {
    type: Schema.Types.Mixed
  },
  status: { type: Number, default: FEED_STATUS.POSTED },
  updated_at: { type: Date },
  social_id: String,
  social_created_by: {
    type: Schema.Types.Mixed,
    default: {
      name: null,
      full_name: null,
      photo_url: null
    }
  }
});

FeedSchema.plugin(autopopulate);

//On the transformation, we just send a simple user, as simple as for social feed that come from facebook, instagram nor twitter
const HASH_CHANGES = {
  "0": (doc, ret, options) => {
    if (ret.created_by && ret.created_by.profile) {
      let created_by = ret.created_by;
      let newCreatedBy = {
        name: created_by.profile.name,
        full_name: created_by.profile.name,
        photo_url: created_by.profile.picture
      };
      ret.created_by = newCreatedBy;
    }else if (ret.social_created_by){
      ret.created_by = ret.social_created_by;
    }

    delete ret.social_created_by;

    //only return the id of the fiche
    //ret.fiche_id = ret.fiche_id.toString();
    //don't need the fiche id so just remove it from the json

    delete ret.__v;

    //find a way to detect if the request comes from editorial inside this model
    delete ret.status;
    delete ret.fiche_id;

    return ret;
  },
  "1": (doc, ret, options) => {
    return ret;
  },
  "1.1": (doc, ret, options) => {

    if (ret && ret.cover) {
      let bigPath = ret.kind === "traverse"? ret.cover.path: ret.cover.big,
          mediumPath = bigPath,
          thumbPath = bigPath;

      if (bigPath && bigPath.indexOf("http") === -1) {
        let splitted = bigPath.split("/");

        let serverPath = recoverFeedBasePath();

        let uid = splitted[2];

        bigPath = serverPath + '/' + uid;
        mediumPath = serverPath + "/medium/" + uid;
        thumbPath  = serverPath + "/thumb/" + uid;
      }

      let previousCover = ret.cover;

      previousCover.path = mediumPath;
      previousCover.big = bigPath;
      previousCover.thumb = thumbPath;

      ret.cover = previousCover;
    }

    return ret;
  },
  "1.2" : (doc, ret, options) => {
    if (ret.social_id) {
      //since we now keep the social id but we don't need to change the model
      ret._id = ret.social_id;
      delete ret.social_id;
    }
    return ret;
  }
};

FeedSchema.options.toJSON = {
  transform: function(doc, ret, options) {
    return performChangesBasedOnApiVersion(doc, ret, options, HASH_CHANGES);
  }
};

export default mongoose.model('Feed', FeedSchema);
