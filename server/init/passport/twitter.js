import TwitterStrategy from 'passport-twitter';
import { twitter } from '../../../config/secrets';
import unsupportedMessage from '../../db/unsupportedMessage';
import { passport as dbPassport } from '../../db';

export default (passport) => {
  if (!dbPassport || !dbPassport.twitter || !typeof dbPassport.twitter === 'function') {
    console.warn(unsupportedMessage('passport-twitter'));
    return;
  }

  /*
  * OAuth Strategy taken modified from google example
  */
  passport.use(new TwitterStrategy({
    consumerKey: twitter.clientID,
    consumerSecret: twitter.clientSecret,
    callbackURL: twitter.callbackURL
  }, dbPassport.twitter));
};
