import InstagramStrategy from 'passport-instagram';
import { instagram } from '../../../config/secrets';
import unsupportedMessage from '../../db/unsupportedMessage';
import { passport as dbPassport } from '../../db';

export default (passport) => {
  if (!dbPassport || !dbPassport.instagram || !typeof dbPassport.instagram === 'function') {
    console.warn(unsupportedMessage('passport-instagram'));
    return;
  }

  /*
  * OAuth Strategy taken modified from google example
  */
  passport.use(new InstagramStrategy({
    clientID: instagram.clientID,
    clientSecret: instagram.clientSecret,
    callbackURL: instagram.callbackURL
  }, dbPassport.instagram));
};
