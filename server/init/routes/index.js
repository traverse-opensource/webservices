/**
 * Routes for express app yolo
 */
import passport from 'passport';
import unsupportedMessage from '../../db/unsupportedMessage';
import { controllers, passport as passportConfig } from '../../db';
import ogExtractor from '../../services/og-extractor.js';
import socialPreviewer from '../../services/previewer.js';

import multer from 'multer' ;
import { socialHandler } from '../../services/socialLogin';
import socialRetriever from '../../services/social';

const uploadMedia = multer( { dest: 'uploaded_images/' } );

//controller block
const usersController = controllers && controllers.users;
const fichesController = controllers && controllers.fiches;
const playlistsController = controllers && controllers.playlists;
const categoriesController = controllers && controllers.categories;
const eventsController = controllers && controllers.events;
const mediaController = controllers && controllers.media;
const objectsController = controllers && controllers.objects;
const placesController = controllers && controllers.places;
const peopleController = controllers && controllers.people;
const tagsController = controllers && controllers.tags;
const themesController = controllers && controllers.themes;
const heartsController = controllers && controllers.heartstrokes;
const groupsController = controllers && controllers.groups;
const entityController = controllers && controllers.entity;
const filtersController = controllers && controllers.filters;
const slugController = controllers && controllers.slugs;
const feedController = controllers && controllers.feeds;
const metaOgController = ogExtractor;
const previewController = socialPreviewer;

//express is needed here to define versions
export default (app, express, serverConfig) => {

  if (passportConfig && passportConfig.google) {
    socialHandler.handleGoogle(app, passport);
  }

  if (passportConfig && passportConfig.facebook) {
    socialHandler.handleFacebook(app, passport);
  }

  if (passportConfig && passportConfig.twitter) {
    socialHandler.handleTwitter(app, passport);
  }

  if (passportConfig && passportConfig.instagram) {
    socialHandler.handleInstragram(app, passport);
  }

  //custom versionning routers
  const apiVersionRouter = express.Router();
  const publicRouter = express.Router();

  //mandatory to append the next urls
  const apiNextRouter = express.Router({mergeParams: true});
  const publicNextRouter = express.Router({mergeParams: true});

  //all the following controller will be versionned

  // user routes
  if (usersController) {
    apiNextRouter
      .get('/users', usersController.getUsers)
      .post('/users/one/login', usersController.login)
      .post('/users/one/signup', usersController.signUp)
      .post('/users/one/logout', usersController.logout)
      .get('/users/:id' , usersController.getUser)
      .get('/users/:id/playlists' , usersController.getUserPlaylists)
      .get('/users/:id/favorites' , usersController.getFavorites)
      .put('/users/:id/favorites' , usersController.addFavorite)
      .delete('/users/:id/favorites' , usersController.removeFavorite)
      .post('/users/:id/syncFavorites' , usersController.syncFavorites)
      //will not allow groups nor role updates from mobile
      .post('/users/:id', usersController.update)
      .get('/users/:id/creation-counts' , usersController.getCreatedItemCount)
      .post('/users/:id/compare-password' , usersController.comparePassword)
      .post('/users/:id/profile-picture' , usersController.updateProfilePicture)
      .post('/users/one/forgotPassword', usersController.forgotPassword)
      .get('/users/one/suggestions', filtersController.search)
      //just need the post request here from mobile
      .post('/users/one/reset/:token', usersController.doReset);
  } else {
    console.warn(unsupportedMessage('users routes'));
  }

  // fiches routes
  if (fichesController) {
    apiNextRouter
      .get('/fiches', fichesController.all)
      .get('/fiches/:id', fichesController.one)
      .get('/fiches/:id/playlists', fichesController.getAssociatedPlaylists)
      .put('/fiches/:id/like', fichesController.like)
      .put('/fiches/:id/dislike', fichesController.dislike)
      //Merge with social retriever the data coming from elsewhere :)
      .get('/fiches/:id/feeds', socialRetriever.all)
      .post('/fiches/:id/feeds', feedController.uploadFeedPicture)
      .get('/fiches/all/stats', fichesController.stat)
      .get('/fiches/all/types', fichesController.getTypes)
  } else {
    console.warn(unsupportedMessage('fiches routes'));
  }

  if (playlistsController) {
    apiNextRouter
      .get('/playlists', playlistsController.all)
      .post('/playlists', playlistsController.add)
      .get('/playlists/:id', playlistsController.one)
      .put('/playlists/:id/like', playlistsController.like)
      .put('/playlists/:id/dislike', playlistsController.dislike)
      .post('/playlists/:id/fullUpdate', playlistsController.fullUpdate)
      .post('/playlists/:id/addLink', playlistsController.addLink)
      .post('/playlists/:id/updateLink', playlistsController.updateLink)
      .post('/playlists/:id/updateCover', playlistsController.updateCover)
      .post('/playlists/:id/updateLabels', playlistsController.updateLabels)
      .post('/playlists/:id/swap', playlistsController.swap)
      .post('/playlists/:id/deleteFiche', playlistsController.deleteFiche)
      .delete('/playlists/:id', playlistsController.remove)
      .put('/playlists/:id', playlistsController.update)
      .put('/playlists/:id/publish', playlistsController.publishPlaylist)
      .put('/playlists/:id/depublish', playlistsController.unPublishPlaylist)
      .get('/playlists/all/stats', playlistsController.stat);
  } else {
    console.warn(unsupportedMessage('playlists routes'));
  }
  if (categoriesController) {
    apiNextRouter
      .get('/categories', categoriesController.all)
      .get('/categories/:id', categoriesController.one);
  } else {
    console.warn(unsupportedMessage('categories routes'));
  }
  if (tagsController) {
    apiNextRouter
      .get('/tags', tagsController.all)
      .get('/tags/:id', tagsController.one)
      //debug routes
      .get('/tags-fix-count', tagsController.updateCount)
      .get('/tags-purge-count', tagsController.purgeCount);
  } else {
    console.warn(unsupportedMessage('tag routes'));
  }
  if(themesController){
    apiNextRouter
      .get('/themes', themesController.all)
      .get('/themes/:id', themesController.one);
      //TODO don't forget to active this route on production only once!
      //.post('/themes', themesController.updateThemeNames);
  }else{
    console.warn(unsupportedMessage('themes routes'))
  }
  if (heartsController){
    apiNextRouter.get('/heartstrokes', heartsController.all);
  }
  //TODO, maybe close this route
  if (groupsController){
    apiNextRouter
      .get('/roles', groupsController.all);
  }

  //TODO maybe close this route
  if (entityController){
    apiNextRouter
      .get('/groups', entityController.all)
      .get('/groups/:id', entityController.one);
  }

  if (filtersController) {
    apiNextRouter.get('/mixins', filtersController.search);
  }

  if (slugController){
    apiNextRouter.get('/slugs', slugController.all);
    //apiNextRouter.get('/slugs/linku-stato', slugController.generateSlugs);
  }

  if(metaOgController){
    apiNextRouter.get('/extractMedia', metaOgController.videoThumb);
  }

  if (socialRetriever) {
    // only available for version >= 1.2
    //apiNextRouter.get('/feeds/debug', socialRetriever.debug);
    apiNextRouter
      //TODO two below are only accessible from editorial
      .get('/feeds/stats', feedController.stats)
      //TODO DEPRECATED wont use this since we really to treat feed status separately
      .get('/feeds/treated', feedController.treated)
      .get('/feeds/non-treated', feedController.nonTreated)
      .get('/feeds/deleted', feedController.deleted)
      .get('/feeds/flagged', feedController.flagged)
      .get('/feeds/posted', feedController.posted)
      .get('/feeds/approved', feedController.approved)
      .put('/feeds/:id/flag', feedController.flag)
      //delete: TODO only for editorial and need at least admin level, TODO would be bad request if trying to use this route from mobile
      .delete('/feeds/:id/:userId', feedController.remove)
      //post: TODO only for editorial and need at least admin level
      .put('/feeds/:id/:userId/post', feedController.post)
      //approve TODO only for editorial and need at least admin level
      .put('/feeds/:id/:userId/approve', feedController.approve);

  }

  //this controller is not protected since it needs to be reachable from external services
  if(previewController){
    publicNextRouter.get('/fiche/:id', socialPreviewer.showFiche);
    publicNextRouter.get('/playlist/:id', socialPreviewer.showPlaylist);
    publicNextRouter.get('/assets/:item', (req, res, next) => {
      res.sendFile(req.originalUrl, { root: __dirname + '/../../../' });
    });
  }

  //just debug for the moment
  apiNextRouter.get('/*json', (req, res) => {
    let toSend = __dirname + '/../../../debug' + req.url;
    return res.send(require(toSend));
  })

  //middleware to check the headers, processed every requests
  apiVersionRouter.use((req, res, next) => {
    console.log(req.headers);

    const keys = Object.keys(serverConfig.keys);
    const secretKeys = keys.map((platform) => {
      return serverConfig.keys[platform];
    })

    let secretKeyHeaderName = serverConfig.headers.secretKey,
        secretKey = req.headers[secretKeyHeaderName];

    //ensures the json is always new nor the css, javascript and other assets
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Last-Modified', (new Date()).toUTCString());
    res.header('Cache-Control', 'public, max-age=1');

    //check if secret key is inside array of secret keys, both for android and iOs
    if (secretKeys.indexOf(secretKey) > -1) {

      //check if request comes from editorial
      req.fromEditorial = false;
      if (req.headers[secretKeyHeaderName] === serverConfig.keys['editorial']) {
        req.fromEditorial = true;
      }


      //find api version here
      let currentRoute = req.originalUrl,
        splitted = currentRoute.split('/'),
        version = splitted[2],
        versionId = version.substr(1);

      req.versionId = parseFloat(versionId).toFixed(1);

      console.log("debug routes", req.originalUrl, req.versionId);

      return next();
    }

    return res.status(400).send("Bad request");
  });

  publicNextRouter.use((req, res, next) => {
    return next();
  });


  //this empty argument is mandatory even if it's not used when merging routers
  apiVersionRouter.use('/v:versionId', apiNextRouter);
  publicRouter.use('/public', publicNextRouter);

  app.use('/api', apiVersionRouter);
  app.use('', publicRouter);
};
