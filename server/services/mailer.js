'use strict';
const nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  host: 'mail.XXX.com',
  port: 587,
  secure: false, // secure:true for port 465, secure:false for port 587
  requireTLS: true,
  auth: {
    user: 'noreply@XXX.com',
    pass: 'XXXXX'
  }
});

const serverAddress = "https://XXXX";

function createMailBody(name, mail, password){
  return  '<div>Bonjour ' + name + '<br/>Un compte a été créé pour vous à ' + serverAddress + ' <br/>Vos identifiants de connection : <br/><ul><li>Identifiant : ' + mail + '</li><li>Mot de passe : ' + password + '</li></ul></div>';
}

/** 
  * params is a json object that contains
  * - user mail
  * - user password
  * - user name
  */
function sendMail(res, params) {
  
  const { name, mail, password } = params;
  
  // setup email data with unicode symbols
  let mailOptions = {
    from: 'Noreply <noreply@XXX.com>', // sender address
    to: name + ' <' + mail + '>', // list of receivers
    subject: 'Compte créé sur XXXX.com', // Subject line
    text: '',
    html: createMailBody(name, mail, password) // html body
  };
  
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
  });
}

export default{
    sendMail
}
