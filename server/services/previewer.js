import format from 'string-template';
import striptags from 'striptags';
import moment from 'moment';
import FicheController from '../db/mongo/controllers/fiches';
import PlaylistController from '../db/mongo/controllers/playlists';
import { FICHE_TYPES } from '../db/mongo/static';

moment.locale('fr');

/* todo default config */
const defaultConfig = {
  siteName: 'Traverse Patrimoines',
  title: '',
  description: '',
  image: '/public/assets/Traverse-logo-small.png',
  url: '',
  type: 'application',
  logo: '/public/assets/Traverse-logo-small.png',
  logoTarget: 'https://www.traverse-patrimoines.com/',
  //intereg will put later on as soon as we have this asset in black
  //logo2: 'public/interreg_france-suisse_noir.png',
  googleUrl: '',
  appleUrl: '',
  //root is important here since we display from /public/playlist|fiche/:id
  googleImg: '/public/assets/google-play-badge.png',
  appleImg: '/public/assets/app-store-badge.svg',
  baseSVGPath: '/public/assets/'
}

function createChipHolder(themeColor, categories, tags) {

  let template =
    '<div class="card-action">'+
      '<p>Labels</p>'+
      '<div class="card-action-inner">'+
        '<div class="chip-holder">{newCats}</div>'+
        '<div class="chip-holder">{newTags}</div>'+
      '</div>'+
    '</div>';

  let values = {
    newCats: categories.map((item) => {
      return '<span class="chip" style="background-color:' + themeColor +'; color: #fff">' + item.name + '</span>';
    }).join(""),
    newTags: tags.map((item) => {
      return '<span class="no-chip" style="color:' + themeColor + ';">#' + item.name + '</span>';
    }).join("")
  }

  return format(template, values);
}

function createPartnersHolder() {
  const partners = {
    facim: {
      name: 'FONDATION FACIM',
      addr: '59 rue du commandant Perceval',
      postal: '73000 Chambéry',
      tel: '+33 (0)4 79 60 59 00',
      fax: '+33 (0)4 79 60 59 01',
      mail: 'info@fondation-facim.fr'
    },
    sipal: {
      name: 'SIPAL',
      addr: 'Place de la Riponne, 10',
      addrComp: 'Section monuments et sites',
      postal: 'CH – 1004 Lausanne',
      tel: '+41(0)21 314 73 00',
      fax: '+41(0)21 316 74 71',
      mail: 'info.sipal@vd.ch'
    }
  }

  const buildOneHolder = (holder) => {
    let toReturn =
      '<div class="col s6">'+
        '<div>'+
          '<p><b>{name}</b></p>'+
          (holder.addrComp ?
            '<p>{addrComp}</p>': '')+
          '<p>{addr}</p>'+
          '<p>{postal}</p>'+
        '</div>'+
        '<div>'+
          '<p><b>Tél.</b>{tel}</p>'+
          '<p><b>Fax</b>{fax}</p>'+
          '<p><a href="mailto:{mail}">{mail}</a></p>'+
        '</div>'+
      '</div>';

    return format(toReturn, holder);
  }

  return buildOneHolder(partners.facim) + buildOneHolder(partners.sipal);
}

function retrieveMixinIcon(mixinItem) {

  let mixinType = mixinItem.type ?
      mixinItem.type: 'playlist';

  return defaultConfig.baseSVGPath + mixinType.toLocaleLowerCase() + '.svg';
}

function buildContent(mixinItem, extractedInfo, isFiche) {

  let categories = isFiche ?
      mixinItem.categories : mixinItem.labels.categories;
  let tags = isFiche?
      mixinItem.tags: mixinItem.labels.tags;

  let template =
  '<header>'+
    '<nav style="background-color: #ffffff">'+
      '<div class="nav-wrapper">'+
        //add link to traverse website and intereg website
        '<a href="{logoTarget}" class="brand-logo">Une carte postale de <img src="{logo}" class="logo"></a>'+
      '</div>'+
    '</nav>'+
  '</header>'+

  '<main>'+
    '<div class="container">'+
      '<div>'+
        '<div class="card">'+
          '<div class="card-image">'+
            '<img class="imgcard center-cropped" style="border-color:{themeColor}; background-image: url({image})">'+
            //'<img class="imgcard center-cropped" style="border-color:{themeColor};" src={image}>'+
            '<div class="picto-card picto-card-{mixinClassType}-holder" style="background-color:{themeColor};">'+
              '<img src={mixinTypeIcon} class="picto-card-{mixinClassType}">'+
              (isFiche?
                '':
              '<span class="playlist-fiche-counter">{nbFiches} fiches</span>') +
            '</div>'+
          '</div>'+
          '<div class="card-content">'+
            '<div style="color: {themeColor}; font-size: 1.4em">{themeName}</div>'+
            '<br/>'+
            '<span class="card-title">{name}</span>'+
            '<p class="overflow-visible">{description}</p>'+
            '<br/>'+
            '<div class="card-action">'+
              '<div class="card-action-inner">'+
                '<center><p class="download-sentence">Pour consulter la {mixinType}, veuillez télécharger l’application mobile</p></center>'+
                '<a href="{googleUrl}" target="_blank"><img class="store" src="{googleImg}"></a>'+
                '<a href="{appleUrl}" target="_blank"><img class="store" style="float:right;" src="{appleImg}"></a>'+
              '</div>'+
            '</div>'+
            '<br/>'+
            '{chips}'+
            '<br/>'+
            '<div class="card-action collection">'+
              '<div class="collection-item avatar">'+
                '<img src="{userProfilePic}" class="circle">'+
                '<p>{fullOrgUserName}</p>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>'+
  '</main>'+

  '<footer class="page-footer z-depth-2" style="background-color: #ffffff; color: #000000">'+
    '<div class="container" style="max-width:640px;">'+
      '<div class="row">'+
        '<div class="col s12">'+
          '<h5 class="">Partenaires</h5>'+
        '</div>'+
      '</div>'+
      '<div class="row">'+
        '<div class="col s12"></div>'+
        '{partners}'+
      '</div>'+
    '</div>'+
    '<div class="footer-copyright" style="color: #000000">'+
      '<div class="container">'+
      '<a href="https://mobilethinking.ch">© {year} MobileThinking</a>'+
      '<a href="https://traverse-patrimoines.com" style="float:right;">Traverse</a>'+
      '</div>'+
    '</div>'+
  '</footer>';

  let { theme } = mixinItem;

  let values = {
    name: extractedInfo.title,
    description: extractedInfo.description,
    image: extractedInfo.image,
    userName: mixinItem.created_by.profile.name,
    userProfilePic: extractedInfo.userProfilePic,
    fullOrgUserName: extractedInfo.fullOrgUserName,
    created_at: moment(mixinItem.created_at).locale('fr'),
    logo: defaultConfig.logo,
    logoTarget: defaultConfig.logoTarget,
    themeName: theme.name,
    themePonderation: theme.ponderation,
    themeColor: theme.color,
    chips: createChipHolder(theme.color, categories, tags),
    googleUrl: defaultConfig.googleUrl,
    appleUrl: defaultConfig.appleUrl,
    googleImg: defaultConfig.googleImg,
    appleImg: defaultConfig.appleImg,
    year: new Date().getFullYear(),
    partners: createPartnersHolder(),
    mixinType: isFiche? "fiche": "playliste",
    mixinTypeIcon: retrieveMixinIcon(mixinItem),
    mixinClassType: mixinItem.type.toLowerCase(),
    nbFiches: isFiche? null: mixinItem.linkedFiches.length
  }

  let toReturn = format(template, values);

  return toReturn;
}

function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}

//extract usefull information based on isFiche and fichetype
function extractDescription(mixinItem, isFiche) {

  const coverPath = mixinItem.cover.path,
        userProfilePic = mixinItem.created_by.profile.picture,
        // NOTE sometimes the user is not assigned to any group, can be a nomad user nor a editorial user but with no entities
        fullOrgUserName = mixinItem.created_by.entities && mixinItem.created_by.entities.length?
          mixinItem.created_by.entities[0].name + ' | ': '';

  let toReturn = {
    title: isFiche? mixinItem.name: "Playlist: " + mixinItem.name,
    description: null,
    //don't forget http for production here
    image: coverPath.indexOf('http') > -1?
      coverPath:
      'https://traverse-patrimoines.com/' + coverPath,
    userProfilePic: userProfilePic.indexOf('http') > -1?
      userProfilePic:
      'https://traverse-patrimoines.com/' + userProfilePic,
    fullOrgUserName: fullOrgUserName + mixinItem.created_by.profile.name
  };

  if (!isFiche) {
    toReturn.description = mixinItem.description;
  }else {
    if (mixinItem.type === FICHE_TYPES.MEDIA) {
      toReturn.description = mixinItem.description;
    }else if (mixinItem.type === FICHE_TYPES.EVENTS) {
      toReturn.description = mixinItem.presentation;
    } else if (mixinItem.type === FICHE_TYPES.OBJECTS || mixinItem.type === FICHE_TYPES.PLACES) {
      toReturn.description = mixinItem.short_description;
    } else {
      toReturn.title = mixinItem.name + ' ' + mixinItem.surname;
      toReturn.description = mixinItem.short_bio;
    }
  }

  //keep without links in ogDescription but adds links in some url are found
  toReturn.ogDescription = striptags(toReturn.description);
  toReturn.description = linkify(toReturn.ogDescription);

  return toReturn;
}

//private, no need to export
function buildPreview(req, res, mixinItem, isFiche = false) {

  let extractedInfo = extractDescription(mixinItem, isFiche),
      content = buildContent(mixinItem, extractedInfo, isFiche),
      config = defaultConfig;

  config.body = content;
  config.title = extractedInfo.title;
  config.description = extractedInfo.ogDescription;
  config.image = extractedInfo.image;
  config.url = req.originalUrl;

  //config.tags = JSON.stringify(tags);

  let string =
      '<!DOCTYPE html>' +
      '<html>' +
        '<head>' +
          '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
          '<meta property="og:site_name" content="{siteName}"/>' +
          '<meta property="og:title" content="{title}"/>' +
          '<meta property="og:description" content="{description}"/>' +
          '<meta property="og:image" content="{image}">' +
          '<meta property="og:url" content="{url}">' +
          '<meta property="og:type" content="{type}"/>' +
          '<meta name="viewport" content="width=device-width">'+
          '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">' +
          '<link rel="icon" href="/public/assets/favicon.png" />'+
          '<link rel="stylesheet" href="/public/assets/preview.css" type="text/css"/>'+
        '</head>' +
        '<body style="background-color: #fafafa">' +
          '{body}' +
          '<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>'+
          '<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>' +
        '</body>' +
      '</html>';

  string = format(string, config);

  return res.status(200).send(string);
}

//warning edited the node module since it need to be fixed to handle when there is a fake link
function showFiche(req, res, next) {
  return FicheController.one(req, res, next, (fiche, type) => {
    buildPreview(req, res, JSON.parse(fiche), true, type);
  });
}

function showPlaylist(req, res, next) {
  return PlaylistController.one(req, res, next, (playlist) => {
    //need the full object that is computed through json process from previous callback
    buildPreview(req, res, JSON.parse(playlist), false, "playlist");
  });
}

//need to fetch the fiche nor the playlist by its id
export default{
  showFiche,
  showPlaylist
}
