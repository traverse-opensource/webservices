import axios from 'axios';
import format from 'string-template';
import Base64 from 'Base64';
import qs from 'qs';
import { twitter } from '../../../config/secrets';
import { FEED_TYPES, FEED_KINDS } from '../../db/mongo/constant';

const traverseUsername = "traverse_patrimoines";

const rcf1738 = (str) => {
  return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\*/g, '%2A')
}

function createBearerHeaders() {

  let rfc1738ConsumerKey = rcf1738(twitter.clientID),
      rfc1738ConsumerSecret = rcf1738(twitter.clientSecret),
      rfcPlain = rfc1738ConsumerKey + ":" + rfc1738ConsumerSecret,
      base64Authorization = format(
        "Basic {base64}",{
          base64: Base64.btoa(rfcPlain)
        }
      );

  return base64Authorization;
}

function retrieveAccessToken(params, callback) {

  let url = "https://api.twitter.com/oauth2/token";

  axios({
    url : url,
    method: "post",
    params: {
      grant_type: "client_credentials"
    },
    headers: {
      "Authorization": createBearerHeaders(),
      "Content-Type": "application/x-www-form-urlencoded"
    }
  })
  .then(function(data) {
    let accessToken = data.data.access_token;
    //console.log("++", accessToken, "++");
    return callback(null, accessToken);
  })
  .catch(function(error){
    return callback(error, null);
  });
}

function transformData(posts) {
  return posts.map((post) => {
    let user = post.user,
        extended = post.extended_entities

    let toReturn = {
      _id: post.id_str,
      type: FEED_TYPES.POST,
      name: "",
      description: post.text,
      permalink: format("https://twitter.com/{stupid}/status/{tweetId}", {
        stupid: traverseUsername,
        tweetId: post.id_str
      }),
      kind: FEED_KINDS.TWITTER,
      //need to transform it to make it compliant with the current database
      created_at: new Date(post.created_at).getTime(),
    };

//TODO entities contains hashtags

    if (user) {
      toReturn.created_by = {
        name: user.screen_name,
        full_name: user.name,
        photo_url: user.profile_image_url
      }
    }

    if (extended && extended.media) {
      let media = extended.media[0];

      //just take only the first media, otherwise too long to be processed
      //TODO need to find the good way to retrieve the public exact image size based on response
      toReturn.cover = {
        big: media.media_url_https,
        path: media.media_url_https,
        thumb: null
      }

      toReturn.type = FEED_TYPES.IMAGE;

      if (media.type === "video") {
        toReturn.type = FEED_TYPES.VIDEO;

        let videoVariant = media.video_info.variants;

        toReturn.video = { big: null, path: null, thumb: null };

        //TODO need to check side effects
        videoVariant.forEach((variant) => {
          if (variant.url.indexOf("320") > -1) {
            toReturn.video.thumb = variant.url;
          }else if (variant.url.indexOf("640") > -1) {
            toReturn.video.path = variant.url;
          } else if (variant.url.indexOf("1280") > -1) {
            toReturn.video.big = variant.url;
          }
        });
      }
    }

    return toReturn;
  }).filter((shorten) => {
    let isMedia = shorten.type !== FEED_TYPES.POST;
    return isMedia;
  });
}

function retrievePost(query, accessToken, callback) {
  const { pageName, limit, next, hashTags } = query;

  let url = "https://api.twitter.com/1.1/statuses/user_timeline.json";

  axios({
    method: 'get',
    url: url,
    params: {
      screen_name: pageName,
      count: limit
    },
    headers: {
      "Authorization": "Bearer " + accessToken,
      "Content-Type": "application/x-www-form-urlencoded"
    },
    //must serialize our query string to match multiple hashtags
    paramsSerializer: function(params) {
       return qs.stringify(params, {arrayFormat: 'repeat'})
    }
  })
  .then(function(data) {
    //console.log("++", data, "++");
    return callback(null, transformData(data.data));
    //return callback(null, data.data);
  })
  .catch(function(e) {
    return callback(e, null);
  })
}

function retrievePostByHashtags(query, accessToken, callback) {
  const { limit, next, hashtags } = query;
  let url = "https://api.twitter.com/1.1/search/tweets.json";

  axios({
    method: 'get',
    url: url,
    params: {
      q: hashtags,
      count: limit
    },
    headers: {
      "Authorization": "Bearer " + accessToken,
      "Content-Type": "application/x-www-form-urlencoded"
    },
    //must serialize our query string to match multiple hashtags
    paramsSerializer: function(params) {
       return qs.stringify(params, {arrayFormat: 'repeat'})
    }
  })
  .then(function(data) {
    //console.log("++", data, "++");
    return callback(null, transformData(data.data.statuses));
    //return callback(null, data.data);
  })
  .catch(function(e) {
    return callback(e, null);
  })
}

function retrieveRemotePostByHahstags(query, callback) {
  return retrieveAccessToken(query, (err, accessToken) => {
    if (err) {
      return callback(err, null);
    }

    return retrievePostByHashtags(query, accessToken, (err, posts) => {
      if (err) {
        return callback(err, null);
      }

      return callback(null, posts);
    });
  });
}

function retrieveRemotePost(query, callback){
  return retrieveAccessToken(query, (err, accessToken) => {
    if (err) {
      return callback(err, null);
    }

    return retrievePost(query, accessToken, (err, posts) => {
      if (err) {
        return callback(err, null);
      }

      return callback(null, posts);
    });
  });
}

module.exports = {
  retrieveRemotePost,
  retrieveRemotePostByHahstags
}
