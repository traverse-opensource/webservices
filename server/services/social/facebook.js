import axios from 'axios';
import format from 'string-template';
import fs from 'fs-extra';
import { facebook } from '../../../config/secrets';
import { FEED_TYPES, FEED_KINDS, MODEL_LIMITS } from '../../db/mongo/constant';


function retrieveAccessToken (params, callback) {
  let args = {
    client_id: facebook.clientID,
    client_secret: facebook.clientSecret,
    grant_type: "client_credentials"
  }

  const checkAccessToken = (tokens) => {
    axios.get("https://graph.facebook.com/oauth/access_token", { params: args })
    .then(function(data) {
      let accessToken = data.data.access_token;

      console.log("in axios retrieving acess toekn", accessToken);
      //update the accessToken and the refresh date
      tokens.facebook = {
        lastRefreshDate: new Date(),
        accessToken: accessToken
      };

      fs.writeJson('config/tokens.json', tokens, (err) => {
        if (err) {
          return callback(err, null);
        }
        return callback(null, accessToken);
      });
    })
    .catch((err) => {
      //console.log("--", err, "--");
      return callback(err, null);
    });
  };


  fs.readJson('config/tokens.json', (err, tokens) => {
    if (err) {
      return callback(err, null);
    }

    console.log(tokens) // => 0.1.3
    let token = tokens.facebook;
    let dateToCompare = new Date().getTime();
    if (token.accessToken == null || dateToCompare > new Date(token.lastRefreshDate).getTime() + MODEL_LIMITS.REFRESH_TOKEN_ELAPSED_TIME) {
      return checkAccessToken(tokens);
    }else {
      return callback(null, token.accessToken);
    }
  })
}

function containsHashtags(remoteHashtags, post) {
  let howManyToMatch = 0,
      { description, message } = post;

  remoteHashtags.forEach((hashtag) => {
    if (description) {
      howManyToMatch += description.indexOf(hashtag) > -1? 1: 0;
    }
    if (message) {
      howManyToMatch += message.indexOf(hashtag) > -1? 1: 0;
    }
  });

  //since can be in the message nor the description
  return howManyToMatch >= remoteHashtags.length;
}

function retrievePost (params, accessToken, callback) {

  console.log("facebook retrieve post", params);

  let pageName = params.pageName,
      limit = params.limit? parseInt(params.limit): 100,
      next = params.next? params.next: null,
      hashtagsToMatch = params.hashtags?
        ( Object.prototype.toString.call( params.hashtags ) === '[object Array]'?
          params.hashtags: [params.hashtags]): [];

  let baseRequest = "https://graph.facebook.com/v2.10/{pageName}/feed?fields=fields=name,message,picture,full_picture,from,description,permalink_url,admin_creator,created_time&limit={limit}&access_token={accessToken}";

  let targetUrl = format(
    baseRequest, {
      pageName: pageName,
      limit: limit,
      accessToken: accessToken
    });

  console.log("++++", targetUrl, "+++++");

  axios({
    url: targetUrl
  })
  .then(function(data) {
    console.log(data.data.length, "yolo ma caille");
    let results = data.data;
    let posts = results.data.filter((post) => {

      return containsHashtags(hashtagsToMatch, post);
    });
    let pagination = results.paging;
    //console.log("... well ...", posts, pagination);
    //console.log("... well ...", pagination);
    //return callback(null, {posts: posts, next: pagination.next});
    return callback(null, posts);
  })
  .catch((err) => {
    //console.log(err, "yolo ma caille");
    return callback(err, null);
  })
}

function transformData(all) {
  return all.map((post) => {
    let user = post.from,
        separator = "<br/><br/>";

    let description =
        (post.description != null? post.description: "") +
        (post.description != null? separator: "" ) +
        (post.message != null? post.message: "");

      //doing the shorten on top level
    //description = shortenDescription(description, MODEL_LIMITS.FEED_DESCRIPTION_LENGTH)

    let toReturn = {
      _id: post.id,
      type: FEED_TYPES.POST,
      name: post.name != null? post.name: "",
      description,
      permalink: post.permalink_url,
      kind: FEED_KINDS.FACEBOOK,
      //need to transform it to make it compliant with the current database
      created_at: new Date(post.created_time).getTime(),
      cover: {
        big: post.full_picture,
        path: post.picture,
        thumb: null
      }
    };

    if (user) {
      toReturn.created_by = {
        name: user.name,
        full_name: user.name,
        photo_url: null
      }
    }

    if (toReturn.permalink) {
      if (toReturn.permalink.indexOf("videos") > -1) {
        toReturn.type = FEED_TYPES.VIDEO;
      }else {
        if (toReturn.cover.path || toReturn.cover.big) {
          toReturn.type = FEED_TYPES.IMAGE;
        }
      }
    }

    return toReturn;
  }).filter((shorten) => {
    let isMedia = shorten.type !== FEED_TYPES.POST;
    return isMedia;
  });
}

function retrieveRemotePost(query, callback){

  return retrieveAccessToken(query, (err, accessToken) => {
    console.log("fb access token", accessToken);
    if (err) {
      return callback(err, null);
    }

    return retrievePost(query, accessToken, (err, all) => {
      if (err) {
        return callback(err, null);
      }

      all = transformData(all);

      return callback(null, all);
    });
  });
}

module.exports = {
  retrieveRemotePost
}

/*
GET https://graph.facebook.com/oauth/access_token?
            client_id=YOUR_APP_ID
           &client_secret=YOUR_APP_SECRET
           &grant_type=client_credentials
*/
