import facebook from './facebook';
import twitter from './twitter';
import instagram from './instagram';
import FeedController from '../../db/mongo/controllers/feeds';

//models to apply versionning
import Feed from '../../db/mongo/models/feeds';
import User from '../../db/mongo/models/users';
import Fiche from '../../db/mongo/models/fiches';

import async from 'async';

import { CONSTANTS, applyModelVersion, REQUEST_MODE, flatten, shortenDescription, createSkipQuery } from '../../db/mongo/static';
import { FEED_KINDS, MODEL_LIMITS, PAGINATION_DEFAULT } from '../../db/mongo/constant';
import FicheController from '../../db/mongo/controllers/fiches';

function uniqueSocialFeed(socialPost, key) {
  var seen = {};
  var out = [];
  var len = socialPost.length;
  var j = 0;
  let toSeek;
  for(var i = 0; i < len; i++) {
    toSeek = socialPost[i].social_id? socialPost[i].social_id: socialPost[i]._id
       //var item = socialPost[i]._id;
       if(seen[toSeek] !== 1) {
             seen[toSeek] = 1;
             out[j++] = socialPost[i];
       }
  }
  return out;
}

//must have a slug if published
function extractSocial(social, kind, limit, slug = null, next = null) {

  let toReturn = [{},{},{},{}],
      pageName = '',
      hashtags = [],
      tags = social.tags? social.tags: [];

  if (social.link) {
    let splitted = social.link.split('/');
    //sometimes the users add a trailing slash, stupid...
    pageName = splitted[splitted.length - 1].length > 0 ? splitted[splitted.length - 1]: splitted[splitted.length - 2];

    if (kind === FEED_KINDS.FACEBOOK) {
      //in case we have page ids
      let innerSplitted = pageName.split('-');
      pageName = innerSplitted.pop();
    }

    toReturn[REQUEST_MODE.NO_HASHTAGS] = {
      pageName: pageName,
      hashtags: null,
      limit: limit
    };
  }


  //no link so we cannot search for an account name
  //if (precondition && social.link.length > 0) {


    hashtags = tags? tags.map((tag) => {
      return '#' + tag.value? tag.value: value.label;
    }): [];

    if (pageName.length > 0 && hashtags.length > 0) {
      toReturn[REQUEST_MODE.HASH_AND_ACCOUNT] = {
        pageName: pageName,
        hashtags, hashtags,
        limit: limit
      };
    }

    //only works within twitter
    if (kind === FEED_KINDS.TWITTER && hashtags.length > 0) {
      toReturn[REQUEST_MODE.MULTIPLE_HASHTAGS] = {
        pageName: null,
        hashtags: hashtags,
        limit
      };

      //this one also only works on twitter
      toReturn[REQUEST_MODE.SLUG] = {
        pageName: null,
        hashtags: [slug],
        limit: limit
      };
    }

  return toReturn;
}

/**
 * Needs at least
 * - pageName
 * - limit
 * Optionnal
 * - hashtags
 * - next
 * sends back an array of object query such as only one for facebook, two insta and twitter because we need to seek for only slug research and pagename against hashtags
 */
function extractSocials (fiche, queryParams = {}) {

  let socials = fiche.social;
  let refreshDates = fiche.refreshDates?
    fiche.refreshDates: {
      instagram: null,
      facebook: null,
      twitter: null
    },
    hasToUpdate = {
      instagram: false,
      facebook: false,
      twitter: false
    }

  fiche.refreshDates = refreshDates;

  const limit = queryParams.limit? parseInt(queryParams.limit): CONSTANTS.SOCIAL.PAGINATION.LIMIT_FEED;

  return {
    instagram: socials.instagram? extractSocial(socials.instagram, FEED_KINDS.INSTAGRAM, limit, fiche.slug): {},
    facebook: socials.facebook? extractSocial(socials.facebook, FEED_KINDS.FACEBOOK, limit): {},
    twitter: socials.twitter? extractSocial(socials.twitter, FEED_KINDS.TWITTER, limit, fiche.slug): {},
    traverse: { fiche_id: fiche._id, refreshDates: refreshDates, fiche: fiche, hasToUpdate: hasToUpdate },
    params: {
      versionId: queryParams.versionId,
      query: queryParams.query
    }
  };
}

//function genericParallel(targetSocial, socialController, updateCallback, upperCallback) {
function genericParallel(targetSocial, socialController, upperCallback) {

  console.log("starting genericParallel for ", targetSocial);

  let socialQueries = queries[targetSocial].length > 0? queries[targetSocial]: [{},{},{},{}];
  let listOfNeededCallbacks = [];

  //console.log("++++++++++++++++++++++++++++++++++",socialQueries, "+++++++++++++++++++++++++++++++++");

  if (Object.keys(socialQueries[REQUEST_MODE.NO_HASHTAGS]).length > 0) {
    listOfNeededCallbacks.push((innerCallback) => {
      socialController.retrieveRemotePost(socialQueries[REQUEST_MODE.NO_HASHTAGS], (err, results) => {
        if (err) {
          //console.log('@facebook social, retrieveRemotePost >', err);
          return innerCallback(null, []);
        }

        setTimeout(() => {return innerCallback(null, results)}, CONSTANTS.SOCIAL.REFRESH_SERIES);
      });
    });
  }

  //multiple hashtags not possible on facebook without page name
  if (Object.keys(socialQueries[REQUEST_MODE.HASH_AND_ACCOUNT]).length > 0) {
    listOfNeededCallbacks.push((innerCallback) => {
      socialController.retrieveRemotePost(socialQueries[REQUEST_MODE.HASH_AND_ACCOUNT], (err, results) => {
        if (err) {
          return innerCallback(null, []);
        }

        setTimeout(() => {innerCallback(null, results)}, CONSTANTS.SOCIAL.REFRESH_SERIES);
      });
    });
  }

  if (Object.keys(socialQueries[REQUEST_MODE.MULTIPLE_HASHTAGS]).length > 0) {
    listOfNeededCallbacks.push((innerCallback) => {
      socialController.retrieveRemotePostByHahstags(socialQueries[REQUEST_MODE.MULTIPLE_HASHTAGS], (err, results) => {
        if (err) {
          return innerCallback(null, []);
        }

        setTimeout(() => {innerCallback(null, results)}, CONSTANTS.SOCIAL.REFRESH_SERIES);
      });
    });
  }

  if (Object.keys(socialQueries[REQUEST_MODE.SLUG]).length > 0) {
    listOfNeededCallbacks.push((innerCallback) => {
      socialController.retrieveRemotePost(socialQueries[REQUEST_MODE.SLUG], (err, results) => {
        if (err) {
          return innerCallback(null, []);
        }

        setTimeout(() => {innerCallback(null, results)}, CONSTANTS.SOCIAL.REFRESH_SERIES);
      });
    });
  }

  //slug is not possible on facebook alone
  async.parallel(listOfNeededCallbacks, function(err, results) {
    if (err) {
      return upperCallback(err, null);
    }

    let toSave = flatten(results);

    console.log("############shall be asyn parallel here", queries.traverse.hasToUpdate[targetSocial], "for", targetSocial, toSave.length, "#########", flatten);

    let hasToUpdate = queries.traverse.hasToUpdate;

    return upperCallback(null, toSave);
  });
}

function genericPreprocess(kind, socialController, callback) {
  let refreshDate = queries.traverse.refreshDates[kind];
  let toCompare = new Date().getTime();

  //console.log("+++++handle------ " + kind + ", refreshDates", refreshDate, queries.traverse.refreshDates, "+++++");

  if (!refreshDate || toCompare > new Date(refreshDate).getTime() + MODEL_LIMITS.REFRESH_SOCIAL_ELAPSED_TIME) {
    return genericParallel(kind, socialController, callback);
  }else {
    //if no need to refresh then just return the ones that are already stored in db
    return callback(null, []);
  }
}

//only parallel methods for all the handles
function handleFacebook(callback) {
  return genericPreprocess(FEED_KINDS.FACEBOOK, facebook, callback);
}

function handleTwitter(callback) {
  return genericPreprocess(FEED_KINDS.TWITTER, twitter, callback);
}

function handleInstagram(callback) {
  return genericPreprocess(FEED_KINDS.INSTAGRAM, instagram, callback);
}

function handleTraverseFeeds(callback) {

  let fakeReq = {
    params: {
      id: queries.traverse.fiche_id
    },
    versionId: queries.params.versionId,
    query: queries.params.query
  },
  fakeRes = {},
  fakeNext = {};

  return FeedController.all(fakeReq, fakeRes, fakeNext, (err, feeds) => {
    if (err) {
      return callback(null, []);
    }else {
      return callback(null, feeds);
    }
  })
}

function handleSocialParalell (finalCallback) {
  async.series([
    handleTraverseFeeds,
    handleFacebook,
    handleInstagram,
    handleTwitter
  ],
  (err, results) => {
    // the results array will equal ['one','two'] even though
    // the second function had a shorter timeout.
    if (err) {
      return finalCallback(err, null);
    }

    //console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", results[0].length,results[1].length,results[2].length,results[3].length);

    //mergin and sorting result
    //TODO do not forget to ensure the resulting list contains only unique id based on their social networks
    let toReturn = [].concat.apply([], results);

    return finalCallback(null, toReturn);
  });
}

let queries = {};

function saveShitInDB(unique, afterSaveCallback) {

  let uniqueIds = [];

  unique.forEach((feed) => {
    if (feed.social_id) {
      if (!uniqueIds[feed.social_id]) {
        uniqueIds[feed.social_id] = 0;
      }
      uniqueIds[feed.social_id]++;
    }else {
      if (!uniqueIds[feed._id]) {
        uniqueIds[feed._id] = 0;
      }
      uniqueIds[feed._id]++;
    }
  });

  let toSave = unique
  //not allowing dupplicates here, earn some db computing time
  //in this case we must have only social feeds
  .filter((feed) => {
    return uniqueIds[feed._id] === 1;
  })

  let counts = {
    'facebook': 0,
    'instagram': 0,
    'traverse': 0,
    'twitter': 0
  };

  toSave.forEach((feed) => {
    counts[feed.kind]++;
  })

  //console.log("putain quoi", counts, uniqueIds);

  let hasToUpdate = {
    instagram: false,
    facebook: false,
    twitter: false,
  }

  let saveList = [];

  toSave.forEach((remoteFeed) => {
    let upsertQuery = {
      social_id: remoteFeed._id,
      name: remoteFeed.name,
      description: remoteFeed.description,
      permalink: remoteFeed.permalink,
      kind: remoteFeed.kind,
      created_at: remoteFeed.created_at,
      social_created_by: remoteFeed.created_by,
      fiche_id: queries.traverse.fiche_id,
      cover: remoteFeed.cover,
      video: remoteFeed.video
    };

    //since it's not a mongo id, we let mongo set automatically the id for the object
    delete upsertQuery._id;

    //a feed can also be linked to another fiche, depends on editorial confiuration so that the
    //upsert query below won't insert if this feed id is already present
    let seekQuery = { social_id: remoteFeed._id };

    saveList.push((saveCallback) => {

      Feed.findOne(seekQuery, (err, feed) => {
        if (err) {
          return saveCallback(err, null);
        }

        if (feed) {
          return saveCallback(err, feed);
        }else {
          let toSave = new Feed(upsertQuery);
          toSave.save((err) => {
            if (err) {
              return saveCallback("new one" + err, null);
            }

            if (!hasToUpdate[toSave.kind]) {
              hasToUpdate[toSave.kind] = true;
            }

            setTimeout(function () {
              return saveCallback(null, toSave);
            }, 75);
          });
        }
      });
    });
  });

  async.series(saveList,
  (err, results) => {
    // the results array will equal ['one','two'] even though
    // the second function had a shorter timeout.
    if (err) {
      //console.log("inside save list callback", err);
      return afterSaveCallback(err, []);//, hasToUpdate);
    }

    queries.traverse.hasToUpdate = hasToUpdate;

    return afterSaveCallback(null, results);//, hasToUpdate);
  });
}

function all(req, res, next) {
  //shall make the request for all the above service and return them as mixed sorting them by creation date
  //handle next and limit later on, just working on this flow for the moment
  const { query } = req;
  //console.log(req.params);
  //console.log(req.query);

  const baseReq = req,
    baseRes = res,
    baseNext = next;

  //FicheController.one(req, res, next, (ficheAsString) => {
  Fiche.findOne({_id: req.params.id}, (err, fiche) => {
    //let fiche = JSON.parse(ficheAsString);

    if (fiche && fiche.status == CONSTANTS.STATUS.PUBLISHED) {

      queries = extractSocials(fiche, {versionId: req.versionId, query: req.query});

      return handleSocialParalell((err, allSocials) => {

        if (err) {
          //TODO console.log("@Social Posts Controller > all > err,", err);
          //console.log("No feeds available");
          return res.status(402).send("Something strange happens, check the logs");
        }

        let unique =
          //allSocials;
          uniqueSocialFeed(allSocials);

        let toReturn = unique
          /*.filter((feed) => {
            return feed != null;// && feed.kind != null;
          })*/
          .map((feed) => {
            feed.created_at = new Date(feed.created_at);
            if (feed.video == null) {
              delete feed.video;
            }
            feed.description = shortenDescription(feed.description, MODEL_LIMITS.FEED_DESCRIPTION_LENGTH);
            return feed;
          }).sort((feedA, feedB) => {
            return feedB.created_at.getTime() - feedA.created_at.getTime();
          });

        saveShitInDB(toReturn.filter((feed) => { return feed.kind !== FEED_KINDS.TRAVERSE; }), (err, list) => {
          ///updating refresh now
          let refreshDates = queries.traverse.fiche.refreshDates;
          let hasToUpdate = queries.traverse.hasToUpdate;

          if (hasToUpdate.instagram) {
            refreshDates.instagram = new Date();
          }
          if (hasToUpdate.facebook) {
            refreshDates.facebook = new Date();
          }
          if (hasToUpdate.twitter) {
            refreshDates.twitter = new Date();
          }


          //refreshDates[kind] = new Date();

          Fiche.findByIdAndUpdate(queries.traverse.fiche_id, { $set: { refreshDates: refreshDates } }, {new: true}, (err, fiche) => {
          //Fiche.findByIdAndUpdate(queries.traverse.fiche_id, { $set: { setField: new Date() } }, {new: true}, (err, fiche) => {
          //Fiche.update({ _id: queries.traverse.fiche._id }, { $set: { refreshDates: { kind: new Date() } } }, (err) => {
            console.log("update callback after updating", err, fiche.refreshDates, queries.traverse.fiche_id, refreshDates);
          });

          applyModelVersion(User, baseReq, { forList: true });
          applyModelVersion(Fiche, baseReq, { forList: true });
          applyModelVersion(Feed, baseReq);

          let
            page = baseReq.query.page? parseInt(baseReq.query.page): PAGINATION_DEFAULT.PAGE,
            limit = baseReq.query.limit? parseInt(baseReq.query.limit): PAGINATION_DEFAULT.LIMIT_FEED,
            skip = createSkipQuery(page, limit);


          return res.json(toReturn.slice(skip, skip + limit));
        });
      });
    }else {
      console.log("@Social Posts Controller > all > fiche is not published");
      return res.status(502).send("Bad request");
    }
  });
}

//TODO this might be conveninent to add all previous
function searchPrevious(req, res, next) {

}

function debug(req, res, next) {
  let query = {
    pageName: 'michaelb4jordan',
    limit: 30
  }
  return twitter.retrieveRemotePost(query, (err, results) => {
    if (err) {
      //TODO console.log('@twitter social, retrieveRemotePost >', err.response.data);

      return res.status(502).send("Cannot perform twitter posts pull for " + query.pageName);
      //return callback("Cannot perform twitter posts pull for " + query.pageName, null);
    }

    return res.json(results);
    //return callback(null, results);
  });
}

export default{
  all,
  debug
}
