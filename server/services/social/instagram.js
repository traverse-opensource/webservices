import axios from 'axios';
import format from 'string-template';
import { instagram } from '../../../config/secrets';
import { FEED_TYPES, FEED_KINDS } from '../../db/mongo/constant';

const LOCAL_CONSTANTS = {
  TYPES: {
    PHOTO: FEED_TYPES.IMAGE,
    VIDEO: FEED_TYPES.VIDEO
  },
  MEDIA_HASH: {
    //640*360
    BIG: "standard_resolution",
    //medium here
    //320*180
    PATH: "low_resolution",
    //150*150
    THUMB: "thumbnail"
  }
};

//solution de secours si jamais le token expire
//TOCHANGE, we will use previous one once we get out of the sandbox
//https://www.instagram.com/lavaux.patrimoine.mondial/media/?max_id=1419758806581301951_1981800998
function retrieveAccessToken (query, callback) {
  return callback(null, instagram.serverAccessToken);
}

function retrievePost (query, accessToken, callback) {
  //by default paginate 20 by 20, if the returned elements size is less than 20, we're at the end of the list
  let baseUrl = "https://www.instagram.com/{accountName}/media";

  if (query.next) {
    baseUrl += "/?max_id={maxId}";
  }

  let url = format(
    baseUrl, {
      accountName: query.pageName,
      maxId: query.next
    }
  );

  console.log("url", baseUrl, url, query);

  axios({
    method: 'get',
    url: url
  })
  /*.then((response) => {
    return response
  })*/
  .then((response) => {
    //console.log("++", response.data, "++");
    return callback(null, response.data.items);
  })
  .catch((error) => {
    console.error("hum", error);
    return callback(error, null);
  })
}

function retrieveRemotePostByHahstags(hashtags, callback) {
  return retrieveAccessToken(query, (err, accessToken) => {
    if (err) {
      return callback(err, null);
    }

    //for the moment we cannot retrieve it unless we go out of the sandbox
    return callback(null, []);
  });
}

function retrieveRemotePost(query, callback){

  /*return retrieveAccessToken(query, (err, accessToken) => {
    if (err) {
      return callback(err, null);
    }*/

    let accessToken = "";

    return retrievePost(query, accessToken, (err, posts) => {
      if (err) {
        return callback(err, null);
      }

      //shorten object structure
      let shortens = posts.map((post) => {

        let user = post.user,
            images = post.images,
            videos = post.videos;

        let toReturn = {
          _id: post.id,
          type: "feed_" + post.type,
          name: "",
          description: post.caption? post.caption.text: "",
          permalink: post.link,
          kind: FEED_KINDS.INSTAGRAM,
          //need to transform it to make it compliant with the current database, it's php time stamp
          created_at: parseFloat(post.created_time) * 1000,
          created_by: {
            name: user.username,
            full_name: user.full_name,
            photo_url: user.profile_picture
          },
          cover: {
            big: images[LOCAL_CONSTANTS.MEDIA_HASH.BIG].url,
            path: images[LOCAL_CONSTANTS.MEDIA_HASH.PATH].url,
            thumb: images[LOCAL_CONSTANTS.MEDIA_HASH.THUMB]? images[LOCAL_CONSTANTS.MEDIA_HASH.THUMB].url: null
          }
        };

        if (post.type === LOCAL_CONSTANTS.TYPES.VIDEO) {
          //sometimes all the video fields are not present
          toReturn.video = {};
          toReturn.video.big = videos[LOCAL_CONSTANTS.MEDIA_HASH.BIG]? videos[LOCAL_CONSTANTS.MEDIA_HASH.BIG].url: null;
          toReturn.video.path = videos[LOCAL_CONSTANTS.MEDIA_HASH.PATH]? videos[LOCAL_CONSTANTS.MEDIA_HASH.PATH].url: null;
          toReturn.video.thumb = videos[LOCAL_CONSTANTS.MEDIA_HASH.THUMB]? videos[LOCAL_CONSTANTS.MEDIA_HASH.THUMB].url: null;
        }

        return toReturn;
      })

      return callback(null, shortens);
    });
  //});
}

module.exports = {
  retrieveRemotePost,
  retrieveRemotePostByHahstags
}
