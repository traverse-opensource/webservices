/**
 * Simple class that helps to login with social networks webservices
 */
const axios = require('axios');
var qs = require('qs');

const socialHandler = {
  
  handleGoogle: function (app, passport) {
    // google auth
    // Redirect the user to Google for authentication. When complete, Google
    // will redirect the user back to the application at
    // /auth/google/return
    // Authentication with google requires an additional scope param, for more info go
    // here https://developers.google.com/identity/protocols/OpenIDConnect#scope-param
    app.get('/auth/google', passport.authenticate('google', {
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    }));

    // Google will redirect the user to this URL after authentication. Finish the
    // process by verifying the assertion. If valid, the user will be logged in.
    // Otherwise, the authentication has failed.
    app.get('/auth/google/callback',
      passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/login'
      })
    );
  },

  handleFacebook: function (app, passport) {
    // facebook auth
    app.get('/auth/facebook', passport.authenticate('facebook', {
      scope: [
        'email',
        'user_likes'
      ]
    }));

    //callback url
    app.get('/auth/facebook/callback',
      passport.authenticate('facebook', {
        successRedirect: '/',
        failureRedirect: '/login'
      })
    );
  },

  handleTwitter: function (app, passport) {
    // twitter auth
    app.get('/auth/twitter', passport.authenticate('twitter'));

    //callback url
    app.get('/auth/twitter/callback',
      passport.authenticate('twitter', { failureRedirect: '/login' }), (req, res) => {
        // Successful authentication, redirect home.
        res.redirect('/');
    });
  },
  
  handleInstragram: function (app, passport) {
    
    app.get('/auth/instagram', passport.authenticate('instagram'));
    
    app.get('/auth/instagram/callback', passport.authenticate('instagram', { failureRedirect: '/debug' }), (req, res, next) => {
      // Successful authentication, send stupid stuff since we need the access token later on, shall be persistant.
      console.log(req.query);
      
      let code = req.query.code;
      
      res.status(200).send({message: "Should be processing by now"});
    });
    
    app.get('/debug', (req, res, next) => {
      console.log("inside debug for whatever reason");
      return res.status(200).send("Moda foka");
    })
  }
}

module.exports = {
  socialHandler
}