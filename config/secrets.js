/** Important **/
/** You should not be committing this file to GitHub **/
/** Repeat: DO! NOT! COMMIT! THIS! FILE! TO! YOUR! REPO! **/
export const sessionSecret = process.env.SESSION_SECRET || 'Your Session Secret goes here';

//TODO make this stuff clearer, since process.env are environment variables that are given on runtime!
//TODO we need to make the process detect if we are in production server nor development one using the same mecanisme for the database detection

export const google = {
  clientID: process.env.GOOGLE_CLIENTID || '',
  clientSecret: process.env.GOOGLE_SECRET || '',
  callbackURL: process.env.GOOGLE_CALLBACK || '/auth/google/callback',
  mapKey: ''
};

export const facebook = {
  clientID: process.env.FACEBOOK_CLIENTID || '',
  clientSecret: process.env.FACEBOOK_SECRET || '',
  callbackURL: process.env.FACEBOOK_CALLBACK || '/auth/facebook/callback'
};

export const twitter = {
  clientID: process.env.FACEBOOK_CLIENTID || '',
  clientSecret: process.env.FACEBOOK_SECRET || '',
  callbackURL: process.env.FACEBOOK_CALLBACK || '/auth/twitter/callback'
};

export const instagram = {
  clientID: process.env.INSTAGRAM_CLIENTID || '',
  clientSecret: process.env.INSTAGRAM_SECRET || '',
  callbackURL: process.env.INSTAGRAM_CALLBACK || '/auth/instagram/callback',
  serverAccessToken: process.env.INSTAGRAM_ACCESS_TOKEN || ''
}